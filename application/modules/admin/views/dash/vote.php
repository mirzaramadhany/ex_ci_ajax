<div class="row">

	<div class="col-sm-12">
		<div class="box box-solid">
			<div class="box-body">
			<form>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
			    			<label for="idactive">Election Year</label>
							<select class="form-control select2" data-sf="load_active" 
							name="idactive" id="idactive" data-placeholder="Election Year" required>
							</select>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="form-group">
			    			<label for="codedistrict">District</label>
							<select class="form-control select2-clear" data-sf="load_district_code" 
							name="codedistrict" id="codedistrict" data-placeholder="District Code" required>
							</select>
						</div>
					</div>
				</div>
			</form>
			</div>			
		</div>
	</div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="fa fa-check"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">Valid Vote</span>
			  <span class="info-box-number" id="idvalid"></span>
			</div>
		</div>
	</div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-green"><i class="fa fa-male"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">Male</span>
			  <span class="info-box-number" id="idmale"></span>
			</div>
		</div>
	</div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-blue"><i class="fa fa-female"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">Female</span>
			  <span class="info-box-number" id="idfemale"></span>
			</div>
		</div>
	</div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-red"><i class="fa fa-heart"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text"><?=$app_party['name']?> Vote</span>
			  <span class="info-box-number" id="idvote"></span>
			</div>
		</div>
	</div>

	<div class="col-sm-12">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Election Results <span class="iddistrict"></span></h3>
			</div>
			<div class="box-body">  
				<div id="cresult"></div>
			</div>
		</div> 
	</div>

	<div class="col-sm-8">
		<div class="box box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Top 4 Party & New Voters <span class="iddistrict"></span></h3>
			</div>
			<div class="box-body">  
				<div id="ctop"></div>
			</div>
		</div> 
	</div>
	<div class="col-sm-4">
		<div class="box box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Top 4 Party <span class="iddistrict"></span></h3>
			</div>
			<div class="box-body" >  
				<div id="cpie"></div>
			</div>
		</div>  
	</div>
</div>

<script type="text/javascript" src="<?=base_url('bismillah/chart/Chart-2.4.0.min.js')?>"></script>
<script type="text/javascript"> 
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>"; 

	bos.dvote.gridresult 	= function(thisdis, thisdata){
		this.obj.find(".iddistrict").html(thisdis) ; 
		this.obj.find("#cresult").html('<canvas id="vs_cresult" width="100%"></canvas>') ;
		this.ccresult = this.obj.find("#vs_cresult")[0].getContext("2d") ;

		this.cresult 	= new Chart( this.ccresult, {
			type: "bar",
		    data: thisdata ,
			options: {
		        legend:{
		        	labels:{
		        		boxWidth: 10, 
		        		generateLabels: function(cdata){
		        			cresult_data = cdata.config.data ;
						    return cresult_data.labels.map(function(label, i) {
						        return {
						            text: label + " : " + parseInt(cresult_data.datasets[0].data[i]).numberformat(),
						            fillStyle : cresult_data.datasets[0].backgroundColor[i]
						        };
						    }, this);
		        		}
		        	}
		        }, 
		        tooltips:{
		        	callbacks: {
		                label: function(tooltipItem, data) {
		                    return " " + parseInt(tooltipItem.yLabel).numberformat() ;
		                }
		            }
		        }
		    }
		}); 

		bjs.ajax( this.base_url + "/loadtop", bjs.getdataform( this.objactive ) ) ; 
	}

	bos.dvote.gridtop 		= function(thisdata, thisdatap){
		this.obj.find("#ctop").html('<canvas id="vs_ctop" width="100%"></canvas>') ;
		this.cctop = this.obj.find("#vs_ctop")[0].getContext("2d") ;

		this.ctop 	= new Chart( this.cctop, {
			type: "bar",
		    data: thisdata ,
			options: {
		        legend:{
		        	labels:{
		        		boxWidth: 10, 
		        		generateLabels: function(cdata){
		        			ctop_data = cdata.config.data ;
						    return ctop_data.labels.map(function(label, i) {
						        return {
						            text: label + " : " + parseInt(ctop_data.datasets[0].data[i]).numberformat(),
						            fillStyle : ctop_data.datasets[0].backgroundColor[i]
						        }; 
						    }, this);
		        		}
		        	}
		        }, 
		        tooltips:{
		        	callbacks: {
		                label: function(tooltipItem, data) {
		                    return " " + parseInt(tooltipItem.yLabel).numberformat() ;
		                }
		            }
		        }
		    }
		}); 

		this.obj.find("#cpie").html('<canvas id="vs_cpie" width="100%"></canvas>') ;
		this.ccpie = this.obj.find("#vs_cpie")[0].getContext("2d") ;

		this.cpie 	= new Chart( this.ccpie, {
			type: "pie", 
		    data: thisdatap ,
			options: {
				animation:{
		            animateScale:true
		        },	
		        legend:{
		        	labels:{
		        		boxWidth: 10, 
		        		generateLabels: function(cdata){
		        			cpie_data = cdata.config.data ;
						    return cpie_data.labels.map(function(label, i) {
						        return {
						            text: label + " : " + parseFloat(cpie_data.datasets[0].data[i]).numberformat(2) + "%",
						            fillStyle : cpie_data.datasets[0].backgroundColor[i]
						        }; 
						    }, this);
		        		}
		        	}
		        }
		    }
		}); 
	}

	bos.dvote.initcomp 		= function(){
		bjs.initselect({
			class 		: "#" + this.id + " .select2"
		}) ;
		
		bjs.initselect({
			class 		: "#" + this.id + " .select2-clear",
			clear 		: true
		}) ;
	}

	bos.dvote.initcallback 	= function(){
		this.obj.find("#idactive, #codedistrict").on("change", function(){
			bos.dvote.initchart() ; 
		}) ; 
	} 

	bos.dvote.objactive 	= bos.dvote.obj.find("#idactive") ; 
	bos.dvote.txt_loading 	= '<div class="loading-text">Loading...</div>' ;
	bos.dvote.initfunc 		= function(){
		setTimeout(function(){
			bos.dvote.obj.find("#idactive").sval(<?=json_encode($idactive)?>) ; 
		},1) ;	 

		setTimeout(function(){
			bos.dvote.initchart() ; 			
		},10 ) ;  
	} 

	bos.dvote.initchart 	= function(){
		bos.dvote.obj.find("#cresult").html(bos.dvote.txt_loading) ;
		bjs.ajax( bos.dvote.base_url + '/loadresult', bjs.getdataform( bos.dvote.objactive ) ) ; 

		bos.dvote.obj.find("#ctop").html(bos.dvote.txt_loading) ;

		bos.dvote.obj.find("#cpie").html(bos.dvote.txt_loading) ;
	}

	$(function(){
		bos.dvote.initcomp() ; 
		bos.dvote.initcallback() ; 
		bos.dvote.initfunc() ; 
	}) ; 
</script>