<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?=$app_title?></title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" type="text/css" href="<?=base_url('bismillah/bootstrap/css/bootstrap.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('bismillah/icon/css/font-awesome.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('bismillah/icon/css/ionicons.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('bismillah/select2/css/select2.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('bismillah/datepicker/bootstrap-datetimepicker.css')?>">
		<link rel="stylesheet" href="<?=base_url('bismillah/w2/w2ui-1.5.rc1.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('bismillah/adminlte/css/AdminLTE.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('bismillah/adminlte/css/skins/skin-gold.css')?>">
		<link rel="stylesheet" href="<?=base_url('bismillah/core_mobile.css')?>">

		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2AgQpLx4g1Xb5MEK7BW_5FG0_f0Kg69Q&libraries=visualization"></script>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-gold sidebar-mini">
		<div class="wrapper">
			<!-- Main Header -->
			<header class="main-header">
				<!-- Logo -->
				<a href="<?=site_url()?>" class="logo">
					<span class="logo-mini"><i class="ion-filing"></i></span>
					<span class="logo-lg"><i class="ion-filing"></i>&nbsp;<?=$app_title?></span>
				</a>
				<!-- Header Navbar -->
				<nav class="navbar navbar-static-top" role="navigation">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<!-- Navbar Right Menu -->
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account Menu -->
							<li class="dropdown user user-menu">
								<!-- Menu Toggle Button -->
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<span class="hidden-xs"><?=$fullname?></span>
								</a>
								<ul class="dropdown-menu">
									<!-- The user image in the menu -->
									<li class="user-header">
										<img src="<?=$data_var['ava']."?time=".time()?>" class="img-circle" alt="User Image" />
										<p><?=$fullname?></p>
									</li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<a href="#" class="btn btn-default btn-flat">Profile</a>
										</div>
										<div class="pull-right">
											<a href="#" onclick="bjs.ajax('admin/frame/logout')" class="btn btn-default btn-flat">Logout</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
					<div class="user-panel">
				        <div class="pull-left image">
				          <img src="<?=$data_var['ava']."?time=".time()?>" class="img-responsive" alt="<?=$fullname?>">
				          <br /> 
				        </div>
				        <div class="pull-left info">
				          <p><?=$fullname?></p>
				          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
				        </div>
				    </div>
					<!-- Sidebar Menu -->
					<ul class="sidebar-menu"><?=$menu_html?></ul>
				</section>
			</aside>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper bwrapper">
				<section class="content-header">
					<h1>Loading, Please Wait</h1>
					<ol class="breadcrumb">
				        <li><a href="#"><i class="fa fa-dashboard"></i> System</a></li>
				        <li class="active">Loading</li>
					</ol>
				</section>
				<section class="content"></section>
			</div>

			<!-- Main Footer -->
			<footer class="main-footer">
				<!-- To the right -->
				<div class="pull-right hidden-xs"><strong>Version</strong> 1.00.00-Beta</div>
				Copyright &copy; 2017 All rights reserved.
			</footer>

		</div>

		<script type="text/javascript" src="<?=base_url('bismillah/jQuery/jquery-2.2.3.min.js')?>"></script>
		<script type="text/javascript" src="<?=base_url('bismillah/bootstrap/js/bootstrap.min.js')?>"></script>
		<script type="text/javascript" src="<?=base_url('bismillah/adminlte/app.js')?>"></script>
		<script type="text/javascript" src="<?=base_url('bismillah/select2/js/select2.full.min.js')?>"></script>
		<script type="text/javascript" src="<?=base_url('bismillah/w2/w2ui-1.5.rc1.min.js')?>"></script>
		<script type="text/javascript" src="<?=base_url('bismillah/datepicker/moment.js')?>"></script>
		<script type="text/javascript" src="<?=base_url('bismillah/datepicker/bootstrap-datetimepicker.min.js')?>"></script>
		<script type="text/javascript" src="<?=base_url('bismillah/jQuery/jquery.number.js')?>"></script>
		
		<script type="text/javascript">
		<?php  
		echo 'var base_url = "'.base_url().'" ;' ;
		?>
		</script>
		<script type="text/javascript" src="<?=base_url('bismillah/core.js')?>"></script>
		<script type="text/javascript">
		<?php
		echo 'bjs.form('.json_encode($oinit).') ; ' ;
		?>
			function form_mobile(par){
				$(".sidebar-menu").find("li.active").removeClass("active") ;
				$(".sidebar-menu").find("li#" + par.md5).addClass("active") ;
				bjs.form(par) ;
			}
		</script>
	</body>
</html>
