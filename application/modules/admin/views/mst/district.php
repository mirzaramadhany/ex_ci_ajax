<div class="row">
	<div class="col-sm-6">
		<div class="box box-info color-palette-box">
		    <div class="box-body">
		    	<form>
		    	<div class="row">
		    		<div class="col-sm-8">
		    			<div class="form-group">
							<label for="cparent">Parent</label>
							<select class="form-control select2 sparent" data-sf="load_district_code" 
							name="cparent" id="cparent" data-placeholder="Parent">
							</select>
		                </div>	
		    		</div>
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="ccode">Code</label>
							<input type="text" class="form-control" name="ccode" id="ccode" placeholder="Code" required>
		                </div>	
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-sm-8">
		    			<div class="form-group">
							<label for="idcode">New Code</label>
							<div id="idcode"></div>
		                </div>
		    		</div>
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="calias">Alias <small style="color:#aaa;">(optional)</small></label>
							<input type="text" class="form-control" name="calias" id="calias" placeholder="Alias" >
		                </div>	
		    		</div>
		    	</div>
		    	<div class="form-group">
					<label for="cname">Name</label>
					<input type="text" class="form-control" name="cname" id="cname" placeholder="Name" required>
                </div>
                <div class="form-group">
					<label for="cdescription">Description</label>
					<input type="text" class="form-control" name="cdescription" id="cdescription" placeholder="Description">
                </div>
                <div class="row">
                	<div class="col-sm-4">
		    			<div class="form-group">
							<label for="isvote">Is Vote</label><br />
							<label class="radio-inline">
								<input type="radio" name="isvote" value="1" checked="true"> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" name="isvote" value="0"> No
							</label>
		                </div>	
		    		</div>
		    	</div>
                <button type="button" id="cmdsave" class="btn btn-primary btn-block">Save</button>
                </form>
			</div>
		</div>	
	</div>

	<div class="col-sm-6">
		<div class="box box-default color-palette-box">
		    <div class="box-body">
		    	<div id="grdistrict" style="height: 400px"></div>
			</div>
		</div>	
	</div>

</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>"; 
	
	bos.mstdistrict.grid1_data 	= null ; 
	bos.mstdistrict.grid1_loaddata= function(){
		val = this.obj.find("#cparent").val() ; 
		if(val == null) val 	= "" ; 
		this.grid1_data 		= {cparent: val} ;
	} 
 
	bos.mstdistrict.grid1_load	= function(){
		this.obj.find("#grdistrict").w2grid({  
	        name	: this.id + '_grid1',  
	        limit 	: 100 ,
	        url 	: bos.mstdistrict.base_url + "/loadgrid",
	        postData: this.grid1_data , 
	        show: {   
	        	footer 		: true,
	        	toolbar		: true
	        },
	        multiSearch		: false, 
	        columns: [    
	            { field: 'code', caption: 'Code', size: '80px', sortable: false },
	            { field: 'name', caption: 'Name', size: '150px', sortable: false },
	            { field: 'isvote', caption: 'Vote', size: '50px', sortable: false },
	            { field: 'cmdedit', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' },
	            { field: 'cmddelete', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' }
	        ]
	    });   
	} 
	bos.mstdistrict.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ; 
	} 
	bos.mstdistrict.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}	
	bos.mstdistrict.grid1_destroy 	= function(){
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ; 
		}
	}
	bos.mstdistrict.grid1_render 	= function(){ 
		this.obj.find("#grdistrict").w2render(this.id + '_grid1') ;  
	}

	bos.mstdistrict.grid1_reloaddata	= function(){
		this.grid1_loaddata() ; 
		this.grid1_setdata() ; 
		this.grid1_reload() ;    
	}

	bos.mstdistrict.cmdedit 		= function(id){
		bjs.ajax(this.base_url + '/editing', 'id=' + id);
	}

	bos.mstdistrict.cmddelete 	= function(id){
		if(confirm("Delete Data?")){
			bjs.ajax(this.base_url + '/deleting', 'id=' + id);		
		}
	}

	bos.mstdistrict.init 			= function(){
		this.obj.find('#ccode').val("").attr("readonly", false).focus() ; 
		this.obj.find("#calias").val("") ;
		this.obj.find("#cname").val("") ;
		this.obj.find("#cdescription").val("") ;
		this.obj.find("#cparent").prop("disabled", false) ;
		bjs.setopt(this.obj, 'isvote', '1') ;

		this.grid1_reloaddata() ;
		bjs.ajax(this.base_url + '/init') ;
	}

	bos.mstdistrict.initcomp		= function(){
		this.grid1_loaddata() ;
		this.grid1_load() ;
		bjs.initselect({
			class 		: "#" + this.id + " .select2.sparent",
			clear 		: true
		}) ;
		bjs.initenter(this.obj) ;

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.mstdistrict.initcallback	= function(){
		this.obj.on('remove', function(){
			bos.mstdistrict.grid1_destroy() ; 
		}) ; 
	}

	bos.mstdistrict.initfunc		= function(){
		setTimeout(function(){
			bos.mstdistrict.obj.find('#ccode').focus() ; 
		},1) ;
		this.obj.find("#cparent").on("select2:select , select2:unselect", function(e){
			setTimeout(function(){
				bos.mstdistrict.obj.find("#ccode").focus() ; 
				bos.mstdistrict.grid1_reloaddata() ;
			},1) ;
		}) ;
		this.obj.find("#cparent").on("change", function(e){
			setTimeout(function(){
				bos.mstdistrict.grid1_reloaddata() ;
			},1) ;
		}) ;
		this.obj.find("#ccode").on("keyup", function(){
			pv 	= bos.mstdistrict.obj.find("#cparent").val() ;
			if(pv == null) pv = "" ; 
			bos.mstdistrict.obj.find("#idcode").html( pv + $(this).val() ) ;
		});
		this.obj.find("#ccode").on("blur", function(){
			if($(this).val() !== ""){
				cdata 	= "newcode=" + bos.mstdistrict.obj.find("#cparent").val() + "" + $(this).val() ; 
				bjs.ajax( bos.mstdistrict.base_url + '/seekcode', cdata ) ; 
			}
		});
		this.obj.find('#cmdsave').on("click", function(){
			if( bjs.isvalidform(this) ){
				bjs.ajax( bos.mstdistrict.base_url + '/saving', bjs.getdataform(this) , this) ;
			}
		}) ;
	}

	$(function(){
		bos.mstdistrict.initcomp() ; 
		bos.mstdistrict.initcallback() ; 
		bos.mstdistrict.initfunc() ; 
	})
</script>