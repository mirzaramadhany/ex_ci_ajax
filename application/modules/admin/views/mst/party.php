<div class="row">
	<div class="col-sm-6">
		<div class="box box-info color-palette-box">
		    <div class="box-body">
		    	<form>
		    	<div class="row">
		    		<div class="col-sm-8">
		    			<div class="form-group">
							<label for="cparent">Parent</label>
							<select class="form-control select2 sparent" data-sf="load_party_code" 
							name="cparent" id="cparent" data-placeholder="Parent">
							</select>
		                </div>	
		    		</div>
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="ccode">Code</label>
							<input type="text" class="form-control" name="ccode" id="ccode" placeholder="Code" required>
		                </div>	
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-sm-8">
		    			<div class="form-group">
							<label for="idcode">New Code</label>
							<div id="idcode"></div>
		                </div>
		    		</div>
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="calias">Alias <small style="color:#aaa;">(optional)</small></label>
							<input type="text" class="form-control" name="calias" id="calias" placeholder="Alias" >
		                </div>	
		    		</div>
		    	</div>
		    	<div class="form-group">
					<label for="cname">Name</label>
					<input type="text" class="form-control" name="cname" id="cname" placeholder="Name" required>
                </div>
                <div class="form-group">
					<label for="cdescription">Description</label>
					<input type="text" class="form-control" name="cdescription" id="cdescription" placeholder="Description" required>
                </div>
                <div class="row">
		    		<div class="col-sm-6">
		    			<div class="form-group">
							<label for="idcode">Image <span id="idlimage"></span></label>
							<input type="file" name="fimage" id="fimage" accept="image/*">
		                </div>
		    		</div>
		    		<div class="col-sm-6" id="idimage" style="text-align:center;"></div>
		    	</div>
                <button type="button" id="cmdsave" class="btn btn-primary btn-block">Save</button>
                </form>
			</div>
		</div>	
	</div>

	<div class="col-sm-6">
		<div class="box box-default color-palette-box">
		    <div class="box-body">
		    	<div id="grparty" style="height: 400px"></div>
			</div>
		</div>	
	</div>

</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>"; 
	
	bos.mstparty.grid1_data 	= null ; 
	bos.mstparty.grid1_loaddata= function(){ 
		val = this.obj.find("#cparent").val() ; 
		if(val == null) val 	= "" ; 
		this.grid1_data 		= {cparent: val} ;
	} 
 
	bos.mstparty.grid1_load	= function(){
		this.obj.find("#grparty").w2grid({  
	        name	: this.id + '_grid1',  
	        limit 	: 100 ,
	        url 	: bos.mstparty.base_url + "/loadgrid",
	        postData: this.grid1_data , 
	        show: {   
	        	footer 		: true
	        },
	        columns: [    
	            { field: 'code', caption: 'Code', size: '80px', sortable: false },
	            { field: 'name', caption: 'Name', size: '150px', sortable: false },
	            { field: 'cmdedit', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' },
	            { field: 'cmddelete', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' }
	        ]
	    });   
	} 
	bos.mstparty.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ; 
	} 
	bos.mstparty.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}	
	bos.mstparty.grid1_destroy 	= function(){
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ; 
		}
	}
	bos.mstparty.grid1_render 	= function(){ 
		this.obj.find("#grparty").w2render(this.id + '_grid1') ;  
	}

	bos.mstparty.grid1_reloaddata	= function(){
		this.grid1_loaddata() ; 
		this.grid1_setdata() ; 
		this.grid1_reload() ;    
	}

	bos.mstparty.cmdedit 		= function(id){
		bjs.ajax(this.base_url + '/editing', 'id=' + id);
	}

	bos.mstparty.cmddelete 	= function(id){
		if(confirm("Delete Data?")){
			bjs.ajax(this.base_url + '/deleting', 'id=' + id);		
		}
	}

	bos.mstparty.init 			= function(){
		this.obj.find('#ccode').val("").attr("readonly", false).focus() ; 
		this.obj.find("#calias").val("") ;
		this.obj.find("#cname").val("") ;
		this.obj.find("#cdescription").val("") ;
		this.obj.find("#cparent").sval();
		this.obj.find("#cparent").prop("disabled", false) ;
		this.obj.find("#idlimage").html("") ;
		this.obj.find("#idimage").html("") ;
		this.obj.find("#idcode").html("") ; 

		this.grid1_reloaddata() ;
		bjs.ajax(this.base_url + '/init') ;
	}

	bos.mstparty.initcomp		= function(){
		this.grid1_loaddata() ;
		this.grid1_load() ;
		bjs.initselect({
			class 		: "#" + this.id + " .select2.sparent",
			clear 		: true
		}) ;
		bjs.initenter(this.obj) ;

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.mstparty.initcallback	= function(){
		this.obj.on('remove', function(){
			bos.mstparty.grid1_destroy() ; 
		}) ; 
	}

	bos.mstparty.initfunc		= function(){
		setTimeout(function(){
			bos.mstparty.obj.find('#ccode').focus() ; 
		},1) ;
		this.obj.find("#cparent").on("select2:select , select2:unselect", function(e){
			setTimeout(function(){
				bos.mstparty.obj.find("#ccode").focus() ; 
				bos.mstparty.grid1_reloaddata() ;
			},1) ;
		}) ;
		this.obj.find("#cparent").on("change", function(e){ 
			setTimeout(function(){
				bos.mstparty.grid1_reloaddata() ;
			},1) ;
		}) ;
		this.obj.find("#ccode").on("keyup", function(){
			pv 	= bos.mstparty.obj.find("#cparent").val() ;
			if(pv == null) pv = "" ; 
			bos.mstparty.obj.find("#idcode").html( pv + $(this).val() ) ;
		});
		this.obj.find("#ccode").on("blur", function(){
			cdata 	= "newcode=" + bos.mstparty.obj.find("#cparent").val() + "" + $(this).val() ; 
			bjs.ajax( bos.mstparty.base_url + '/seekcode', cdata ) ; 
		});
		this.obj.find("#fimage").on("change", function(e){
			e.preventDefault() ;

            bos.mstparty.cfile    = e.target.files ;
            bos.mstparty.gfile    = new FormData() ;
            $.each(bos.mstparty.cfile, function(cKey,cValue){
              bos.mstparty.gfile.append(cKey,cValue) ; 
            }) ;         

            bos.mstparty.obj.find("#idlimage").html("<i class='fa fa-spinner fa-pulse'></i>");
            bos.mstparty.obj.find("#idimage").html("") ;

            bjs.ajaxfile(bos.mstparty.base_url + "/saving_image", bos.mstparty.gfile, this) ;

		})
		this.obj.find('#cmdsave').on("click", function(){
			if( bjs.isvalidform(this) ){
				bjs.ajax( bos.mstparty.base_url + '/saving', bjs.getdataform(this) , this) ;
			}
		}) ;
	}

	$(function(){
		bos.mstparty.initcomp() ; 
		bos.mstparty.initcallback() ; 
		bos.mstparty.initfunc() ; 
	})
</script>