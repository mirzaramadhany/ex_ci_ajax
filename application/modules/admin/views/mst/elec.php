<div class="row">
	<div class="col-sm-6">
		<div class="box box-info color-palette-box">
		    <div class="box-body">
		    	<form>
		    	<div class="form-group">
					<label for="cname">Name</label>
					<input type="text" class="form-control" name="cname" id="cname" placeholder="Name" required>
                </div>
                <div class="form-group">
					<label for="cdescription">Description</label>
					<input type="text" class="form-control" name="cdescription" id="cdescription" placeholder="Description" required>
                </div>
                <div class="row">
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="dstart">Date Start</label>
							<input type="text" class="form-control date" name="dstart" id="dstart" data-date-format="DD-MM-YYYY HH:mm"
							placeholder="Date Start" required>
		                </div>	
		    		</div>
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="dend">Date End</label>
							<input type="text" class="form-control date" name="dend" id="dend" data-date-format="DD-MM-YYYY HH:mm"
							placeholder="Date End" required>
		                </div>	
		    		</div>
		    	</div>
                <button type="button" id="cmdsave" class="btn btn-primary btn-block">Save</button>
                </form>
			</div>
		</div>	
	</div>

	<div class="col-sm-6">
		<div class="box box-default color-palette-box">
		    <div class="box-body">
		    	<div id="grelec" style="height: 400px"></div>
			</div>
		</div>	
	</div>

</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>"; 
	
	bos.mstelec.grid1_data 	= null ; 
	bos.mstelec.grid1_loaddata= function(){ 
	} 
 
	bos.mstelec.grid1_load	= function(){
		this.obj.find("#grelec").w2grid({  
	        name	: this.id + '_grid1',  
	        limit 	: 100 ,
	        url 	: bos.mstelec.base_url + "/loadgrid",
	        postData: this.grid1_data , 
	        show: {   
	        	footer 		: true
	        },
	        columns: [    
	            { field: 'name', caption: 'Name', size: '150px', sortable: false },
	            { field: 'date_start', caption: 'Start', size: '100px', sortable: false },
	            { field: 'date_end', caption: 'End', size: '100px', sortable: false },
	            { field: 'cmdedit', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' },
	            { field: 'cmddelete', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' }
	        ]
	    });   
	} 
	bos.mstelec.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ; 
	} 
	bos.mstelec.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}	
	bos.mstelec.grid1_destroy 	= function(){
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ; 
		}
	}
	bos.mstelec.grid1_render 	= function(){ 
		this.obj.find("#grelec").w2render(this.id + '_grid1') ;  
	}

	bos.mstelec.grid1_reloaddata	= function(){
		this.grid1_loaddata() ; 
		this.grid1_setdata() ; 
		this.grid1_reload() ;    
	}

	bos.mstelec.cmdedit 		= function(id){
		bjs.ajax(this.base_url + '/editing', 'id=' + id);
	}

	bos.mstelec.cmddelete 	= function(id){
		if(confirm("Delete Data?")){
			bjs.ajax(this.base_url + '/deleting', 'id=' + id);		
		}
	}

	bos.mstelec.init 			= function(){
		this.obj.find("#cname").val("") ;
		this.obj.find("#cdescription").val("") ;
		this.obj.find("#dstart").val("") ;
		this.obj.find("#dend").val("") ;

		this.grid1_reloaddata() ;
		bjs.ajax(this.base_url + '/init') ;
	}

	bos.mstelec.initcomp		= function(){
		this.grid1_loaddata() ;
		this.grid1_load() ;
		bjs.initenter(this.obj) ;
		bjs.initdate("#" + this.id + " .date", true) ; 

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.mstelec.initcallback	= function(){
		this.obj.on('remove', function(){
			bos.mstelec.grid1_destroy() ; 
		}) ; 
	}

	bos.mstelec.initfunc		= function(){
		setTimeout(function(){
			bos.mstelec.obj.find('#cname').focus() ; 
		},1) ;
		this.obj.find('#cmdsave').on("click", function(){
			if( bjs.isvalidform(this) ){
				bjs.ajax( bos.mstelec.base_url + '/saving', bjs.getdataform(this) , this) ;
			}
		}) ;
	}

	$(function(){
		bos.mstelec.initcomp() ; 
		bos.mstelec.initcallback() ; 
		bos.mstelec.initfunc() ; 
	})
</script>