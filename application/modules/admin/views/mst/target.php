<div class="row">
	<div class="col-sm-6">
		<div class="box box-info color-palette-box">
		    <div class="box-body">
		    	<form>
		    	<div class="form-group">
					<label for="name">Name</label>
					<input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                </div>
                <div class="form-group">
					<label for="cdescription">Description</label>
					<input type="text" class="form-control" name="description" id="description" placeholder="Description">
                </div>
                <div class="row">
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="target">Vote Target</label>
							<input type="text" class="form-control number" name="target" id="target"
							placeholder="Target" required value="300000">
		                </div>	
		    		</div>
		    	</div>
                <button type="button" id="cmdsave" class="btn btn-primary btn-block">Save</button>
                </form>
			</div>
		</div>	
	</div>
 
	<div class="col-sm-6">
		<div class="box box-default color-palette-box">
		    <div class="box-body">
		    	<div id="grtarget" style="height: 400px"></div>
			</div>
		</div>	
	</div>

</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>"; 
	
	bos.msttarget.grid1_data 	= null ; 
	bos.msttarget.grid1_loaddata= function(){ 
	} 
 
	bos.msttarget.grid1_load	= function(){
		this.obj.find("#grtarget").w2grid({  
	        name	: this.id + '_grid1',  
	        limit 	: 100 ,
	        url 	: bos.msttarget.base_url + "/loadgrid",
	        postData: this.grid1_data , 
	        show: {   
	        	footer 		: true
	        },
	        columns: [    
	            { field: 'name', caption: 'Name', size: '150px', sortable: false },
	            { field: 'target', caption: 'Target', size: '100px', sortable: false,style:'text-align:right;' },
	            { field: 'cmdedit', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' },
	            { field: 'cmddelete', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' }
	        ]
	    });   
	} 
	bos.msttarget.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ; 
	} 
	bos.msttarget.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}	
	bos.msttarget.grid1_destroy 	= function(){ 
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ; 
		}
	}
	bos.msttarget.grid1_render 	= function(){ 
		this.obj.find("#grtarget").w2render(this.id + '_grid1') ;  
	}

	bos.msttarget.grid1_reloaddata	= function(){
		this.grid1_loaddata() ; 
		this.grid1_setdata() ; 
		this.grid1_reload() ;    
	}

	bos.msttarget.cmdedit 		= function(id){
		bjs.ajax(this.base_url + '/editing', 'id=' + id);
	}

	bos.msttarget.cmddelete 	= function(id){
		if(confirm("Delete Data?")){
			bjs.ajax(this.base_url + '/deleting', 'id=' + id);		
		}
	}

	bos.msttarget.init 			= function(){
		this.obj.find("#name").val("") ;
		this.obj.find("#description").val("") ;
		this.obj.find("#target").val("") ;

		this.grid1_reloaddata() ;
		bjs.ajax(this.base_url + '/init') ;
	}

	bos.msttarget.initcomp		= function(){
		this.grid1_loaddata() ;
		this.grid1_load() ;
		bjs.initenter(this.obj) ;
		bjs.initnumber("#" + this.id + " .number") ; 

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.msttarget.initcallback	= function(){
		this.obj.on('remove', function(){
			bos.msttarget.grid1_destroy() ; 
		}) ; 
	}

	bos.msttarget.initfunc		= function(){
		setTimeout(function(){
			bos.msttarget.obj.find('#name').focus() ; 
		},1) ;
		this.obj.find('#cmdsave').on("click", function(){
			if( bjs.isvalidform(this) ){
				bjs.ajax( bos.msttarget.base_url + '/saving', bjs.getdataform(this) , this) ;
			}
		}) ;
	}

	$(function(){
		bos.msttarget.initcomp() ; 
		bos.msttarget.initcallback() ; 
		bos.msttarget.initfunc() ; 
	})
</script>