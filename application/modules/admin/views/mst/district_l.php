<div class="row">
	<div class="col-sm-6">
		<div class="box box-info color-palette-box">
		    <div class="box-body">
		    	<form>
		    	<div class="form-group">
					<label for="cname">Name</label>
					<input type="text" class="form-control" name="cname" id="cname" placeholder="Name" required>
                </div>
                <div class="row">
                	<div class="col-sm-4">
                		<div class="form-group">
							<label for="nlen">Length</label>
							<input type="text" class="form-control" name="nlen" id="nlen" placeholder="Length" maxlength="2"
							style="text-align:right" required>
		                </div>	
                	</div>
                </div>
                <button type="button" id="cmdsave" class="btn btn-primary btn-block">Save</button>
                </form>
			</div>
		</div>	
	</div>

	<div class="col-sm-6">
		<div class="box box-default color-palette-box">
		    <div class="box-body">
		    	<div id="grdistrict_l" style="height: 400px"></div>
			</div>
		</div>	
	</div>

</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>"; 

	bos.mstdist_l.grid1_data 	 = null ; 
	bos.mstdist_l.grid1_loaddata= function(){
	} 

	bos.mstdist_l.grid1_load	= function(){
		this.obj.find("#grdistrict_l").w2grid({  
	        name	: this.id + '_grid1',  
	        limit 	: 100 ,
	        url 	: bos.mstdist_l.base_url + "/loadgrid",
	        postData: this.grid1_data , 
	        show: {   
	        	footer 		: true
	        },
	        columns: [    
	            { field: 'val', caption: 'Len', size: '50px', sortable: false },
	            { field: 'title', caption: 'Name', size: '150px', sortable: false },
	            { field: 'cmdedit', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' },
	            { field: 'cmddelete', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' }
	        ]
	    });   
	} 
	bos.mstdist_l.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ; 
	} 
	bos.mstdist_l.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}	
	bos.mstdist_l.grid1_destroy 	= function(){
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ; 
		}
	}
	bos.mstdist_l.grid1_render 	= function(){ 
		this.obj.find("#grdistrict_l").w2render(this.id + '_grid1') ;  
	}

	bos.mstdist_l.grid1_reloaddata	= function(){
		this.grid1_loaddata() ; 
		this.grid1_setdata() ; 
		this.grid1_reload() ;    
	}

	bos.mstdist_l.cmdedit 		= function(id){
		bjs.ajax(this.base_url + '/editing', 'id=' + id);
	}

	bos.mstdist_l.cmddelete 	= function(id){
		if(confirm("Delete Data?")){
			bjs.ajax(this.base_url + '/deleting', 'id=' + id);		
		}
	}

	bos.mstdist_l.init 			= function(){
		this.obj.find("#cname").val("") ;
		this.obj.find("#nlen").val("") ;
		this.obj.find("#cname").focus() ;
		this.grid1_reloaddata() ;
		bjs.ajax(this.base_url + '/init') ;
	}

	bos.mstdist_l.initcomp		= function(){
		this.grid1_loaddata() ;
		this.grid1_load() ;

		bjs.initenter(this.obj) ;

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.mstdist_l.initcallback	= function(){
		this.obj.on('remove', function(){
			bos.mstdist_l.grid1_destroy() ; 
		}) ; 
	}

	bos.mstdist_l.initfunc		= function(){
		setTimeout(function(){
			bos.mstdist_l.obj.find('#cname').focus() ; 
		},1) ;
		this.obj.find('#cmdsave').on("click", function(){
			if( bjs.isvalidform(this) ){
				bjs.ajax( bos.mstdist_l.base_url + '/saving', bjs.getdataform(this) , this) ;
			}
		}) ; 
	}

	$(function(){
		bos.mstdist_l.initcomp() ; 
		bos.mstdist_l.initcallback() ; 
		bos.mstdist_l.initfunc() ; 
	})
</script>