<div class="row">
	<div class="col-sm-6">
		<div class="box box-info color-palette-box">
		    <div class="box-body">
		    	<form>
		    	<div class="row">
		    		<div class="col-sm-6">
		    			<div class="form-group">
			    			<label for="idactive">Election Date</label>
							<select class="form-control select2" data-sf="load_active" 
							name="idactive" id="idactive" data-placeholder="Election Date" required>
							</select>
						</div>
		    		</div>
		    		<div class="col-sm-6">
		    			<div class="form-group">
			    			<label for="idparty">Party</label>
							<select class="form-control select2" data-sf="load_party" 
							name="idparty" id="idparty" data-placeholder="Party" required>
							</select>
						</div>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-sm-6">
		    			<div class="form-group">
			    			<label for="iddistrict">District</label>
							<select class="form-control select2" data-sf="load_district" 
							name="iddistrict" id="iddistrict" data-placeholder="District" required>
							</select>
						</div>
		    		</div>
		    		<div class="col-sm-6">
		    			<div class="form-group">
							<label for="vote">Vote</label>
							<input type="text" class="form-control number" name="vote" id="vote" placeholder="Vote" value="0" required style="font-size: 24px">
		                </div>
		    		</div>
		    	</div>	

                <div class="form-group">
					<label for="description">Description</label>
					<input type="text" class="form-control" name="description" id="description" placeholder="Description" >
                </div>
                <button type="button" id="cmdsave" class="btn btn-primary btn-block">Save</button>
                </form>
			</div>
		</div>	
	</div>

	<div class="col-sm-6">
		<div class="box box-default color-palette-box">
		    <div class="box-body">
		    	<div id="grvote" style="height: 400px"></div>
			</div>
		</div>	
	</div>

</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>"; 
	
	bos.trvote.grid1_data 	= null ; 
	bos.trvote.grid1_loaddata= function(){
		val = this.obj.find("#idactive").val() ; 
		vp 	= this.obj.find("#idparty").val() ;
		if(val == null) val 	= "" ; 
		if(vp == null) vp 		= "" ; 
		this.grid1_data 		= {idactive: val, idparty: vp} ;
	} 
 
	bos.trvote.grid1_load	= function(){
		this.obj.find("#grvote").w2grid({  
	        name	: this.id + '_grid1',  
	        limit 	: 100 ,
	        url 	: bos.trvote.base_url + "/loadgrid",
	        postData: this.grid1_data , 
	        show: {   
	        	footer 		: true,
	        	toolbar		: true
	        },
	        multiSearch		: false, 
	        columns: [    
	        	{ field: 'active', caption: 'Election', size: '150px', sortable: false },
	        	{ field: 'party', caption: 'Party', size: '150px', sortable: false },
	            { field: 'district', caption: 'District', size: '150px', sortable: false },
	            { field: 'vote', caption: 'Vote', size: '100px', sortable: false,style:'text-align:right;' },
	            { field: 'cmdedit', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' },
	            { field: 'cmddelete', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' }
	        ]
	    });   
	} 
	bos.trvote.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ; 
	} 
	bos.trvote.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}	
	bos.trvote.grid1_destroy 	= function(){
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ; 
		}
	}
	bos.trvote.grid1_render 	= function(){ 
		this.obj.find("#grvote").w2render(this.id + '_grid1') ;  
	}

	bos.trvote.grid1_reloaddata	= function(){
		this.grid1_loaddata() ; 
		this.grid1_setdata() ; 
		this.grid1_reload() ;    
	}

	bos.trvote.cmdedit 		= function(id){
		bjs.ajax(this.base_url + '/editing', 'id=' + id);
	}

	bos.trvote.cmddelete 	= function(id){
		if(confirm("Delete Data?")){
			bjs.ajax(this.base_url + '/deleting', 'id=' + id);		
		}
	}

	bos.trvote.init 			= function(){
		this.obj.find("#vote").val("0") ;
		this.obj.find("#description").val("") ;
		this.obj.find("#iddistrict").sval() ; 

		this.grid1_reloaddata() ;

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.trvote.initcomp		= function(){
		this.grid1_loaddata() ;
		this.grid1_load() ;
		bjs.initselect({
			class 		: "#" + this.id + " .select2"
		}) ;
		bjs.initnumber("#" + this.id + " .number") ;
		bjs.initenter(this.obj) ;

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.trvote.initcallback	= function(){
		this.obj.on('remove', function(){
			bos.trvote.grid1_destroy() ; 
		}) ; 
	}

	bos.trvote.initfunc		= function(){
		this.obj.find("#idparty, #idactive").on("change", function(e){
			setTimeout(function(){
				bos.trvote.grid1_reloaddata() ;
			},1) ;
		}) ;
		this.obj.find("#idparty").on("select2:selecting", function(e){
			setTimeout(function(){
				bos.trvote.obj.find("#iddistrict").select2("open") ;
			},1) ;
		}) ;
		this.obj.find("#iddistrict").on("select2:selecting", function(e){
			setTimeout(function(){
				bos.trvote.obj.find("#vote").focus() ; 
			},1) ;
		});
		this.obj.find('#cmdsave').on("click", function(){
			if( bjs.isvalidform(this) ){
				bjs.ajax( bos.trvote.base_url + '/saving', bjs.getdataform(this) , this) ;
			}
		}) ;
	}

	$(function(){
		bos.trvote.initcomp() ; 
		bos.trvote.initcallback() ; 
		bos.trvote.initfunc() ; 
	})
</script>