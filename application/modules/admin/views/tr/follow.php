<div class="row">
	<div class="col-sm-6">
		<div class="box box-info color-palette-box">
		    <div class="box-body">
		    	<form>
		    	<div class="row">
		    		<div class="col-sm-2">
		    			<div class="form-group">
							<label for="calias">Year</label>
							<input type="text" class="form-control" name="year" id="year" placeholder="Year" required 
							style="text-align: center" value="<?=date("Y")?>">
		                </div>
		    		</div>
		    		<div class="col-sm-10">
		    			<div class="form-group">
			    			<label for="idparty">Party</label>
							<select class="form-control select2" data-sf="load_party" 
							name="idparty" id="idparty" data-placeholder="Party" required>
							</select>
						</div>
		    		</div>	
		    		<div class="col-sm-8">
		    			<div class="form-group">
							<label for="iddistrict">District</label>
							<select class="form-control select2" data-sf="load_district_parent" 
							name="iddistrict" id="iddistrict" data-placeholder="District" required>
							</select>
		                </div>
		    		</div>
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="follower">Militant</label>
							<input type="text" class="form-control number" name="follower" id="follower" placeholder="Militant"
							value="0" required style="font-size: 24px">
		                </div>
		    		</div>
		    	</div>
                <div class="form-group">
					<label for="description">Description</label>
					<input type="text" class="form-control" name="description" id="description" placeholder="Description">
                </div>
                <button type="button" id="cmdsave" class="btn btn-primary btn-block">Save</button>
                </form>
			</div>
		</div>	
	</div>

	<div class="col-sm-6">
		<div class="box box-default color-palette-box">
		    <div class="box-body">
		    	<div id="grfollow" style="height: 400px"></div>
			</div>
		</div>	
	</div>

</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>"; 
	
	bos.trfollow.grid1_data 	= null ; 
	bos.trfollow.grid1_loaddata= function(){
		val = this.obj.find("#year").val() ; 
		vp 	= this.obj.find("#idparty").val() ;
		if(val == null) val 	= "" ; 
		if(vp == null) vp 		= "" ; 
		this.grid1_data 		= {year: val, idparty: vp} ;
	}  
 
	bos.trfollow.grid1_load	= function(){
		this.obj.find("#grfollow").w2grid({  
	        name	: this.id + '_grid1',  
	        limit 	: 100 ,
	        url 	: bos.trfollow.base_url + "/loadgrid",
	        postData: this.grid1_data , 
	        show: {   
	        	footer 		: true,
	        	toolbar		: true
	        },
	        multiSearch		: false, 
	        columns: [    
	        	{ field: 'party', caption: 'Party', size: '150px', sortable: false},
	        	{ field: 'year', caption: 'Year', size: '50px', sortable: false,style:'text-align:center;' },
	            { field: 'district', caption: 'District', size: '150px', sortable: false },
	            { field: 'follower', caption: 'Militant', size: '75px', sortable: false,style:'text-align:right;' },
	            { field: 'cmdedit', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' },
	            { field: 'cmddelete', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' }
	        ]
	    });   
	} 
	bos.trfollow.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ; 
	} 
	bos.trfollow.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}	
	bos.trfollow.grid1_destroy 	= function(){
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ; 
		}
	}
	bos.trfollow.grid1_render 	= function(){ 
		this.obj.find("#grfollow").w2render(this.id + '_grid1') ;  
	}

	bos.trfollow.grid1_reloaddata	= function(){
		this.grid1_loaddata() ; 
		this.grid1_setdata() ; 
		this.grid1_reload() ;    
	}

	bos.trfollow.cmdedit 		= function(id){
		bjs.ajax(this.base_url + '/editing', 'id=' + id);
	}

	bos.trfollow.cmddelete 	= function(id){
		if(confirm("Delete Data?")){
			bjs.ajax(this.base_url + '/deleting', 'id=' + id);		
		}
	}

	bos.trfollow.init 			= function(){
		this.obj.find("#year").val("<?=date("Y")?>") ;
		this.obj.find("#follower").val("0") ;
		this.obj.find("#description").val("") ;
		this.obj.find("#iddistrict").sval() ; 
		this.obj.find("#iddistrict").prop("disabled", false) ;  
		this.obj.find("#idparty").prop("disabled", false) ;  

		this.grid1_reloaddata() ;

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.trfollow.initcomp		= function(){
		this.grid1_loaddata() ;
		this.grid1_load() ;
		bjs.initselect({
			class 		: "#" + this.id + " .select2"
		}) ;
		bjs.initnumber("#" + this.id + " .number") ;
		bjs.initenter(this.obj) ; 

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.trfollow.initcallback	= function(){
		this.obj.on('remove', function(){
			bos.trfollow.grid1_destroy() ; 
		}) ; 
	}

	bos.trfollow.initfunc		= function(){
		this.obj.find("#iddistrict, #year, #idparty").on("change", function(e){
			setTimeout(function(){
				bos.trfollow.grid1_reloaddata() ;
			},1) ;
		}) ;
		this.obj.find("#idparty").on("select2:selecting", function(e){
			setTimeout(function(){
				bos.trfollow.obj.find("#iddistrict").select2("open") ;
			},1) ;
		}) ;
		this.obj.find("#iddistrict").on("select2:selecting", function(e){
			setTimeout(function(){
				bos.trfollow.obj.find("#follower").focus() ; 
			},1) ;
		})
		this.obj.find('#cmdsave').on("click", function(){
			if( bjs.isvalidform(this) ){
				bjs.ajax( bos.trfollow.base_url + '/saving', bjs.getdataform(this) , this) ;
			}
		}) ;
	}

	$(function(){
		bos.trfollow.initcomp() ; 
		bos.trfollow.initcallback() ; 
		bos.trfollow.initfunc() ; 
	})
</script>