<div class="row">
	<div class="col-sm-6">
		<div class="box box-info color-palette-box">
		    <div class="box-body">
		    	<form>
		    	<div class="row">
		    		<div class="col-sm-2">
		    			<div class="form-group">
							<label for="calias">Year</label>
							<input type="text" class="form-control" name="year" id="year" placeholder="Year" required 
							style="text-align: center" value="<?=date("Y")?>">
		                </div>
		    		</div>
		    		<div class="col-sm-10">
		    			<div class="form-group">
							<label for="iddistrict">District</label>
							<select class="form-control select2" data-sf="load_district" 
							name="iddistrict" id="iddistrict" data-placeholder="District" required>
							</select>
		                </div>		
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="voters">Voters</label>
							<input type="text" class="form-control number" name="voters" id="voters" placeholder="Voters" 
							value="0" required>
		                </div>
		    		</div>
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="vote_valid">Valid Vote</label>
							<input type="text" class="form-control number" name="vote_valid" id="vote_valid" placeholder="Valid Vote"
							value="0" required>
		                </div>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="male">Male</label>
							<input type="text" class="form-control number plus-gender" name="male" id="male" placeholder="Male"
							value="0" required>
		                </div>	
		    		</div>
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="female">Female</label>
							<input type="text" class="form-control number plus-gender" name="female" id="female" placeholder="Female"
							value="0" required>
		                </div>
		    		</div>
		    		<div class="col-sm-4">
		    			<div class="form-group">
							<label for="vote_valid">Population</label>
							<div id="idpop" style="text-align: right; font-size: 16px; padding-top: 5px">0</div>
		                </div>
		    		</div>
		    	</div>
                <div class="form-group">
					<label for="description">Description</label>
					<input type="text" class="form-control" name="description" id="description" placeholder="Description">
                </div>
                <button type="button" id="cmdsave" class="btn btn-primary btn-block">Save</button>
                </form>
			</div>
		</div>	
	</div>

	<div class="col-sm-6">
		<div class="box box-default color-palette-box">
		    <div class="box-body">
		    	<div id="grddistrict" style="height: 400px"></div>
			</div>
		</div>	
	</div>

</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>"; 
	
	bos.trdistrict.grid1_data 	= null ; 
	bos.trdistrict.grid1_loaddata= function(){
		val = this.obj.find("#iddistrict").val() ; 
		vy 	= this.obj.find("#year").val() ;
		if(val == null) val 	= "" ; 
		if(vy == null) vy 		= "" ; 
		this.grid1_data 		= {iddistrict: val, year: vy} ;
	} 
 
	bos.trdistrict.grid1_load	= function(){
		this.obj.find("#grddistrict").w2grid({  
	        name	: this.id + '_grid1',  
	        limit 	: 100 ,
	        url 	: bos.trdistrict.base_url + "/loadgrid",
	        postData: this.grid1_data , 
	        show: {   
	        	footer 		: true,
	        	toolbar		: true
	        },
	        multiSearch		: false, 
	        columns: [    
	        	{ field: 'year', caption: 'Year', size: '50px', sortable: false,style:'text-align:center;' },
	            { field: 'district', caption: 'District', size: '150px', sortable: false },
	            { field: 'voters', caption: 'Voters', size: '75px', sortable: false,style:'text-align:right;' },
	            { field: 'vote_valid', caption: 'Valid', size: '75px', sortable: false,style:'text-align:right;' },
	            { field: 'male', caption: 'Male', size: '75px', sortable: false,style:'text-align:right;' },
	            { field: 'female', caption: 'Female', size: '75px', sortable: false,style:'text-align:right;' },
	            { field: 'population', caption: 'Population', size: '75px', sortable: false,style:'text-align:right;' },
	            { field: 'cmdedit', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' },
	            { field: 'cmddelete', caption: ' ', size: '80px', sortable: false,style:'text-align:center;' }
	        ]
	    });   
	} 
	bos.trdistrict.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ; 
	} 
	bos.trdistrict.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}	
	bos.trdistrict.grid1_destroy 	= function(){
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ; 
		}
	}
	bos.trdistrict.grid1_render 	= function(){ 
		this.obj.find("#grddistrict").w2render(this.id + '_grid1') ;  
	}

	bos.trdistrict.grid1_reloaddata	= function(){
		this.grid1_loaddata() ; 
		this.grid1_setdata() ; 
		this.grid1_reload() ;    
	}

	bos.trdistrict.cmdedit 		= function(id){
		bjs.ajax(this.base_url + '/editing', 'id=' + id);
	}

	bos.trdistrict.cmddelete 	= function(id){
		if(confirm("Delete Data?")){
			bjs.ajax(this.base_url + '/deleting', 'id=' + id);		
		}
	}

	bos.trdistrict.init 			= function(){
		this.obj.find("#voters").val("0") ;
		this.obj.find("#vote_valid").val("0") ;
		this.obj.find("#male").val("0") ;
		this.obj.find("#female").val("0") ;
		this.obj.find("#description").val("") ;
		this.obj.find("#idpop").html("0") ;
		this.obj.find("#iddistrict").sval() ; 
		this.obj.find("#iddistrict").prop("disabled", false) ;  

		this.grid1_reloaddata() ;

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.trdistrict.initcomp		= function(){
		this.grid1_loaddata() ;
		this.grid1_load() ;
		bjs.initselect({
			class 		: "#" + this.id + " .select2"
		}) ;
		bjs.initnumber("#" + this.id + " .number") ;
		bjs.initenter(this.obj) ;

		bjs.ajax(this.base_url + '/init') ;
	}

	bos.trdistrict.initcallback	= function(){
		this.obj.on('remove', function(){
			bos.trdistrict.grid1_destroy() ; 
		}) ; 
	}

	bos.trdistrict.initfunc		= function(){
		this.obj.find("#iddistrict").on("select2:select , select2:unselect", function(e){
			setTimeout(function(){
				bos.trdistrict.obj.find("#voters").focus() ; 
				bos.trdistrict.grid1_reloaddata() ;
			},1) ;
		}) ;
		this.obj.find("#iddistrict, #year").on("change", function(e){
			setTimeout(function(){
				bos.trdistrict.grid1_reloaddata() ;
			},1) ;
		}) ;
		this.obj.find('#cmdsave').on("click", function(){
			if( bjs.isvalidform(this) ){
				bjs.ajax( bos.trdistrict.base_url + '/saving', bjs.getdataform(this) , this) ;
			}
		}) ;
		this.obj.find(".plus-gender").on("keyup", function(){
			n 	= parseInt( bos.trdistrict.obj.find("#male").val() ) +  
					parseInt( bos.trdistrict.obj.find("#female").val() ) ;
			bos.trdistrict.obj.find("#idpop").html( n.numberformat() ) ;
		}) ; 
	}

	$(function(){
		bos.trdistrict.initcomp() ; 
		bos.trdistrict.initcallback() ; 
		bos.trdistrict.initfunc() ; 
	})
</script>