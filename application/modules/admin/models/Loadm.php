<?php 
class Loadm extends Bismillah_Model{
	public function district_code($q){
		$where 		= "code LIKE '".$this->escape_like_str($q)."%' OR name LIKE '%".$this->escape_like_str($q)."%'" ; 
		$dbdata 	= $this->select("mst_district", "code, name", $where, "", "", "codeparent ASC", "0,5") ; 
		return $dbdata ; 
	}

	public function district_code_nodetail($q){
		$where 		= "(code LIKE '".$this->escape_like_str($q)."%' OR name LIKE '%".$this->escape_like_str($q)."%') AND LENGTH(code) < 6" ;
		$dbdata 	= $this->select("mst_district", "code, name", $where, "", "", "code ASC", "0,5") ; 
		return $dbdata ; 
	}

	public function district_parent($q){ 
		$where 		= "(code LIKE '".$this->escape_like_str($q)."%' OR name LIKE '%".$this->escape_like_str($q)."%') AND idparent IS NULL"; 
		$dbdata 	= $this->select("mst_district", "id, code, name", $where, "", "", "codeparent ASC", "0,5") ; 
		return $dbdata ; 
	}	

	public function district($q){
		$where 		= "code LIKE '".$this->escape_like_str($q)."%' OR name LIKE '%".$this->escape_like_str($q)."%'" ; 
		$dbdata 	= $this->select("mst_district", "id, code, name", $where, "", "", "codeparent ASC", "0,5") ; 
		return $dbdata ; 
	}	

	public function party($q){
		$where 		= "code LIKE '".$this->escape_like_str($q)."%' OR name LIKE '%".$this->escape_like_str($q)."%'" ; 
		$dbdata 	= $this->select("mst_party", "id, code, name", $where, "", "", "codeparent ASC", "0,5") ; 
		return $dbdata ; 
	}	

	public function active($q){
		$where 		= "name LIKE '%".$this->escape_like_str($q)."%'" ; 
		$dbdata 	= $this->select("mst_active", "id, name", $where, "", "", "id DESC", "0,5") ; 
		return $dbdata ; 
	}

	public function target($q){
		$where 		= "name LIKE '%".$this->escape_like_str($q)."%' OR target LIKE '%".$this->escape_like_str($q)."%'" ; 
		$dbdata 	= $this->select("mst_target", "name, target", $where, "", "", "id DESC", "0,5") ;
		return $ddbda; 	
	}
 
	public function level($q){
		$where 		= "name LIKE '%".$this->escape_like_str($q)."%' OR code LIKE '%". $this->escape_like_str($q) ."%'" ;
		$dbdata 	= $this->select("sys_username_level", "name, code", $where , "", "", "code DESC", "0,5") ; 
		return $dbdata ;
	}
}
?>