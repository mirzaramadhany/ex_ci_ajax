<?php
class Districtm extends Bismillah_Model{
	public function grid($va){
		$limit	= $va['offset'].",".$va['limit'] ; //limit
		$search	= isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
		$where 	= array() ; 
		if($va['cparent'] !== ""){ 
			$where[]	= "code LIKE '".$this->escape_like_str($va['cparent'])."%'" ; 
		}
		if($search !== ""){
			$where[]	= "(code LIKE '".$this->escape_like_str($search)."%' OR name LIKE '%".$this->escape_like_str($search)."%')" ; 	
		} 
		$where 	= implode(" AND ", $where) ; 
		$dbdata = $this->select("mst_district", "id, code, name, isvote", $where, "", "", "code ASC", $limit) ;
		$dba 	= $this->select("mst_district", "id", $where) ;

		return array("db"=>$dbdata, "rows"=> $this->rows($dba) ) ;
	}

	public function saving($id, $va){
		$data 	= array("alias"=>$va['calias'], "name"=>$va['cname'], "description"=>$va['cdescription'],
						"isvote"=>$va['isvote'], "username"=>getsession($this, "username") ) ;
		if($id == ""){
			if(!isset($va['cparent'])) $va['cparent']	= "" ; 
			$data['code']	= $va['cparent'] . $va['ccode'] ; 
			$data['codeme']	= $va['ccode'] ;
			$data['codeparent']	= $va['cparent'] ;  	
		} 
		$where 	= "id = " . $this->escape($id) ;
		$this->update("mst_district", $data, $where, "id") ;
	}

	public function getdata($id){
		$data 	= array() ;
		if($data= $this->getval("*", "id = " . $this->escape($id), "mst_district")){
			$oparent 	= array() ;
			$parent 	= $this->districtm->getval("name", "id = " . $this->escape($data['idparent']), "mst_district") ;
			if($parent !== ""){
				$oparent[] 	= array("id"=>$data['codeparent'], "text"=>$data['codeparent'] . " - " . $parent) ;
			}
			$data['oparent']= $oparent ; 
		}
		return $data ; 
	}

	public function seekcode($newcode){
		return $this->getval("code", "code = " . $this->escape($newcode), "mst_district") ; 
	} 

	public function deleting($id){
		$this->districtm->delete("mst_district", "id = " . $id) ;
	}
}
?>