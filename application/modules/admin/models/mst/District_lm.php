<?php
class District_lm extends Bismillah_Model{
	public function grid($va){
		$limit	= $va['offset'].",".$va['limit'] ; //limit
		$search	= isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
		$where 	= array() ; 
		if($va['cparent'] !== ""){ 
			$where[]	= "code LIKE '".$this->escape_like_str($va['cparent'])."%'" ; 
		}
		if($search !== ""){
			$where[]	= "(code LIKE '".$this->escape_like_str($search)."%' OR name LIKE '%".$this->escape_like_str($search)."%')" ; 	
		} 
		$where 	= implode(" AND ", $where) ; 
		$dbdata = $this->select("mst_district", "id, code, name, isvote", $where, "", "", "code ASC", $limit) ;
		$dba 	= $this->select("mst_district", "id", $where) ;

		return array($dbdata, $dba) ;
	}
}
?>