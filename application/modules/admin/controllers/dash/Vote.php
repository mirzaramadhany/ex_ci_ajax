<?php
class Vote extends Bismillah_Controller{
	private $color 	= array("#3c76bd", "#a43534", "#99c04e", "#7957a2", "#39aac8", "#f68c3e", "#bd3d3b",
							"#96bc4c", "#7b58a4", "#3baecd", "#12af53", "#fd1a20", "#c23f3d", "#9cc34f",
							"#7957a2", "#3badcd", "#fe9141", "#3d79c1", "#c23f3d", "#99bf4e", "#7b58a4" ) ;
	public function __construct(){
		parent::__construct() ; 
		$this->load->model('dash/votem.php') ;
	}
	
	public function index(){
		$data 	= $this->votem->getindex();
		$data['app_party']	= getsession($this, "app_party") ; 
		$this->load->view("dash/vote", $data) ; 
	}

	public function loadresult(){
		$va 	= $this->input->post() ;
		$data = array("labels"=>array(), 
						"datasets"=>array( 
							array("label"=>"Vote" ,"data"=>array(), 
								"backgroundColor"=>$this->color ) 
							) 
						) ; 

		$vawin 	= array('order'=>array(), 'data'=>array()) ; 
		$dis 	= $this->bdb->getval("name", "id = '{$va['idactive']}'", "mst_active") ; 
		$where 	= array() ; 
		$where[]= "active_id = '{$va['idactive']}'" ;
		$d 		= isset($va['codedistrict']) ? $va['codedistrict'] : "" ; 
		if($d !== ""){
			$where[]	= "district_code LIKE '$d%'" ; 
			$dis 	   .= ' \"'.$this->bdb->getval("name", "code = '$d'", "mst_district").'\"'; 
		}
		$where 	= implode(" AND ", $where) ; 
		$field 	= "party_id, party_name, SUM(vote) avote" ; 
		$dbdata = $this->bdb->select("v_vote", $field, $where, "", "party_id") ; 
		while($dbrow 	= $this->bdb->getrow($dbdata)){
			$data['labels'][]	= $dbrow['party_name'] ;  
			$data['datasets'][0]['data'][]	= $dbrow['avote'] ;

			$vawin['order'][$dbrow['party_id']]	= $dbrow['avote'] ;
			$vawin['data'][$dbrow['party_id']]	= $dbrow ; 
		}
		savesession($this, "sdvote_result", json_encode($vawin)) ;
		echo('
			
		') ;

	}

	function loadtop(){
		$va 	= $this->input->post() ;
		$c 		= array("labels"=>array(), 
						"datasets"=>array( 
							array("label"=>"Vote" ,"data"=>array(), 
								"backgroundColor"=>array() ) 
							) 
						) ; 

		$app_party 	= getsession($this, "app_party") ; 
		$idparty	= $app_party['id'] ; 

		$data 	= json_decode(getsession($this, "sdvote_result"), true) ; 
		arsort($data['order']) ; 

		$s 		= array("labels"=>array("","","","","",""), "bg"=>array("","","","","",""), "data"=>array(0,0,0,0,0,0) ) ; 
		$n 		= 0 ;
		$nt 	= 0 ;
		$np 	= 0 ; 
		foreach ($data['order'] as $akey => $arow) {
			$row 			= $data['data'][$akey] ; 
			$avote 			= intval($row['avote']) ;
			if($row['party_id'] == $idparty) $np += intval($avote) ;

			$nt 			   += $avote ;  
			$s['labels'][$n]	= ($n < 4) ? $row['party_name'] : "Other Party";
			$s['bg'][$n]		= ($n < 4) ? $this->color[intval($row['party_id'])-1] : "#34495e" ;
			$s['data'][$n]	   += $avote ;
			if($n < 4) $n++ ;
		}


		//ambil perbedaan data
		$year 		= date("Y") ; 
		$datayear 	= array("voters"=>0, "vote_valid"=>0, "male"=>0, "female"=>0) ; 
		
		
		$dbdata 	= $this->bdb->select("mst_district_data", "year", "", "", "year", "year DESC", "0,1") ;
		if($dbrow 	= $this->bdb->getrow($dbdata)){
			$year 	= $dbrow['year'] ; 
			if($dby = $this->election->get_total_district_data($year)){
				$datayear	= $dby ; 
			}
		}

		$s['labels'][5]	= "New Voters " . $year ; 
		$s['bg'][5]		= "#bdc3c7" ;  
		$s['data'][5]	= intval($datayear['voters']) - intval($nt) ; 

		$c['labels']	= $s['labels'] ; 
		$c['datasets'][0]['backgroundColor']= $s['bg'] ;
		$c['datasets'][0]['data']			= $s['data'] ;

		//pie
		$p 	= array() ; 
		foreach ($s['data'] as $key => $value) {
			if($key < 5){
				$p[]	= round( $value / $nt * 100, 2) ; 
			}
		}

		$cp = $c;
		unset($cp['labels'][5]) ; unset($cp['datasets'][0]['backgroundColor'][5]) ;
		$cp['datasets'][0]['data']	= $p ; 
 
		echo('
			bos.dvote.obj.find("#idvalid").html("'.number_format($nt).'") ; 
			bos.dvote.obj.find("#idmale").html("'.number_format($nt).'") ; 
			bos.dvote.obj.find("#idfemale").html("0") ; 
			bos.dvote.obj.find("#idvote").html("'.number_format($np). " (" . number_format( $np / $nt * 100 ,2) .'%)") ; 

		') ;

	}

}
?> 