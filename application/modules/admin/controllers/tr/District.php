<?php
	class District extends Bismillah_Controller{
		public function __construct(){
			parent::__construct() ; 
		}

		public function index(){
			$this->load->view("tr/district.php") ;
		}

		public function loadgrid(){
			$va	 	= json_decode($this->input->post('request'), true) ;
			$vasum 	= array("voters"=>0, "vote_valid"=>0, "male"=>0, "female"=>0, "population"=>0) ;
			$vare 	= array() ; 
			$limit	= $va['offset'].",".$va['limit'] ; //limit
			$search	= isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
			$where 	= array() ; 
			if($va['iddistrict'] !== "") $where[]	= "dd.iddistrict LIKE '{$va['iddistrict']}%'" ; 
			if($va['year'] !== "") $where[]			= "dd.year = '{$va['year']}'" ; 
			if($search !== "") $where[]	= "d.code LIKE '{$search}%'" ; 
			$where 	= implode(" AND ", $where) ; 
			$join 	= "LEFT JOIN mst_district d ON d.id = dd.iddistrict" ; 
			$field 	= "dd.id, CONCAT(d.code, ' ', d.name) district, dd.year, dd.voters, dd.vote_valid, dd.male, dd.female, dd.population" ; 
			$dbdata = $this->bdb->select("mst_district_data dd", $field, $where, $join, "", "id DESC", $limit) ;
			$dba 	= $this->bdb->select("mst_district_data dd", "dd.id", $where, $join, "", "id DESC") ;
			while( $dbrow	= $this->bdb->getrow($dbdata) ){
				$vaset 		= $dbrow ; unset($vaset['id']) ; 
				$vaset['recid']	= $dbrow['id'] ; 
				$vaset['voters']	= number_format($vaset['voters']) ; 
				$vaset['vote_valid']= number_format($vaset['vote_valid']) ;
				$vaset['male']		= number_format($vaset['male']) ;
				$vaset['female']	= number_format($vaset['female']) ;
				$vaset['population']= number_format($vaset['population']) ;

				$vaset['cmdedit'] 	= '<button type="button" onClick="bos.trdistrict.cmdedit(\''.$dbrow['id'].'\')" 
										class="btn btn-success btn-grid">Edit</button>' ;
				$vaset['cmddelete'] = '<button type="button" onClick="bos.trdistrict.cmddelete(\''.$dbrow['id'].'\')" 
										class="btn btn-danger btn-grid">Delete</button>' ; 
				$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
				$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

				$vare[]		= $vaset ;  
			}

			if($va['offset'] == 0){
				$vas 	= array("w2ui"=>array("summary"=>true), "recid"=>"s1") ; 
				$field 	= "SUM(dd.voters) voters, SUM(dd.vote_valid) vote_valid, SUM(dd.male) male, SUM(dd.female) female, 
							SUM(dd.population) population" ; 
				$dbdata = $this->bdb->select("mst_district_data dd", $field, $where, $join);
				if($dbrow = $this->bdb->getrow($dbdata)){
					foreach ($vasum as $key => $value) {
						$vas[$key]	= number_format($dbrow[$key]) ; 
					} 	
				}
				$vare[]	= $vas ; 
			}

			$vare 	= array("total"=> $this->bdb->rows($dba), "records"=>$vare ) ;
			echo(json_encode($vare)) ; 
		}

		public function init(){
			savesession($this, "ssddistrict_id", "") ;
		}

		public function saving(){
			$va 	= $this->input->post() ;
			$id 	= getsession($this, "ssddistrict_id") ;
			$data 	= array("iddistrict"=>$va['iddistrict'], "year"=>$va['year'], "voters"=>$va['voters'], "vote_valid"=>$va['vote_valid'], 
							"male"=>$va['male'], "female"=>$va['female'], "description"=>$va['description']	) ; 
			$this->bdb->update("mst_district_data", $data, "id = '{$id}'", "id") ;
			echo(' bos.trdistrict.init() ; ') ;
			
		}

		public function editing(){
			$va 	= $this->input->post() ; 
			$data 	= $this->bdb->getval("*", "id = '{$va['id']}'", "mst_district_data") ;
			if(!empty($data)){
				savesession($this, "ssddistrict_id", $va['id']) ;
				$odistrict 	= array() ;
				$district 	= $this->bdb->getval("name, code", "id = '{$data['iddistrict']}'", "mst_district") ;
				if( !empty( $district ) ){
					$odistrict[] 	= array("id"=>$data['iddistrict'], "text"=>$district['code'] . " - " . $district['name']) ;
				}
				echo('	with(bos.trdistrict.obj){
							find("#year").val("'.$data['year'].'") ;
							find("#voters").val("'.number_format($data['voters']).'") ;
							find("#vote_valid").val("'.number_format($data['vote_valid']).'") ;
							find("#male").val("'.number_format($data['male']).'") ;
							find("#female").val("'.number_format($data['female']).'") ; 
							find("#idpop").html("'.number_format($data['population']).'") ;
							find("#iddistrict").sval('.json_encode($odistrict).') ; 
							find("#iddistrict").prop("disabled", false) ; 
						}
				') ;
			}
		}

		public function deleting(){
			$va 	= $this->input->post() ;
			$this->bdb->delete("mst_district_data", "id = '{$va['id']}'") ;
			echo(' bos.trdistrict.init() ; ') ; 
		}
	}
?>