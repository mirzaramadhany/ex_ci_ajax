<?php
	class Follow extends Bismillah_Controller{
		public function __construct(){
			parent::__construct() ; 
		}

		public function index(){
			$this->load->view("tr/follow.php") ;
		}

		public function loadgrid(){
			$va	 	= json_decode($this->input->post('request'), true) ;
			$vare 	= array() ; 
			$nfollow = 0 ;
			$limit	= $va['offset'].",".$va['limit'] ; //limit
			$where 	= array() ; 
			if($va['year'] !== "") $where[]	= "f.year = '{$va['year']}'" ; 
			if($va['idparty'] !== "") $where[]		= "f.idparty = '{$va['idparty']}'" ; 
			$search	= isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
			if($search !== "") $where[]	= "d.code LIKE '{$search}%'" ; 
			$where 	= implode(" AND ", $where) ;  
			$field 	= "f.id, CONCAT(d.code, ' ' , d.name) district, CONCAT(p.code, ' ' , p.name) party, f.follower, f.year" ; 
			$join 	= "	LEFT JOIN mst_district d ON d.id = f.iddistrict 
						LEFT JOIN mst_party p ON p.id = f.idparty" ; 
			$dbdata = $this->bdb->select("mst_party_follower f", $field, $where, $join, "", "d.code ASC", $limit) ;
			$dba 	= $this->bdb->select("mst_party_follower f", "f.id", $where, $join) ; 
			while( $dbrow	= $this->bdb->getrow($dbdata) ){ 
				$vaset 		= $dbrow ; unset($vaset['id']) ; 
				$vaset['recid']		= $dbrow['id'] ; 
				$vaset['follower']	= number_format($vaset['follower']) ; 

				$vaset['cmdedit'] 	= '<button type="button" onClick="bos.trfollow.cmdedit(\''.$dbrow['id'].'\')" 
										class="btn btn-success btn-grid">Edit</button>' ;
				$vaset['cmddelete'] = '<button type="button" onClick="bos.trfollow.cmddelete(\''.$dbrow['id'].'\')" 
										class="btn btn-danger btn-grid">Delete</button>' ; 
				$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
				$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

				$vare[]		= $vaset ;  
			} 

			if($va['offset'] == "0"){
				$dbdata = $this->bdb->select("mst_party_follower f", "SUM(f.follower) follower", $where, $join) ; 
				if($dbrow	= $this->bdb->getrow($dbdata)){
					$vare[]	= array("w2ui"=>array("summary"=>true), "recid"=>"s1", 
									"follower"=>number_format($dbrow['follower'])) ; 
				}
			}

			$vare 	= array("total"=> $this->bdb->rows($dba), "records"=>$vare ) ;
			echo(json_encode($vare)) ; 
		}

		public function init(){
			savesession($this, "ssfollower_id", "") ;
		}

		public function saving(){
			$va 	= $this->input->post() ;
			$id 	= getsession($this, "ssfollower_id") ;
			$data 	= array("iddistrict"=>$va['iddistrict'], "idparty"=>$va['idparty'], "follower"=>$va['follower'], 
							"description"=>$va['description'], "year"=>$va['year'], 
							"username"=> getsession($this, "username")) ; 
			$this->bdb->update("mst_party_follower", $data, "id = '{$id}'", "id") ;
			echo(' bos.trfollow.init() ; ') ;
			
		}

		public function editing(){
			$va 	= $this->input->post() ; 
			$data 	= $this->bdb->getval("*", "id = '{$va['id']}'", "mst_party_follower") ;
			if(!empty($data)){
				savesession($this, "ssfollower_id", $va['id']) ;
				$odistrict 	= array() ;
				$district 	= $this->bdb->getval("code, name", "id = '{$data['iddistrict']}'", "mst_district") ;
				if(!empty($district)){
					$odistrict[] 	= array("id"=>$data['iddistrict'], "text"=>$district['code'] . " - " . $district['name']) ;
				}

				$oparty 	= array() ; 
				$party	 	= $this->bdb->getval("code, name", "id = '{$data['idparty']}'", "mst_party") ;
				if(!empty($party)){
					$oparty[] 	= array("id"=>$data['idparty'], "text"=>$party['code'] . " - " . $party['name']) ;
				}

				echo('	with(bos.trfollow.obj){
							find("#follower").val("'.number_format($data['follower']).'") ;
							find("#description").val("'.$data['description'].'") ;
							find("#year").val("'.$data['year'].'") ;
							find("#iddistrict").sval('.json_encode($odistrict).') ;  
							find("#idparty").sval('.json_encode($oparty).') ; 
							find("#idparty").prop("disabled", false) ; 
						}
				') ;
			}
		}

		public function deleting(){
			$va 	= $this->input->post() ;
			$this->bdb->delete("mst_party_follower", "id = '{$va['id']}'") ;
			echo(' bos.trfollow.init() ; ') ; 
		}
	}
?>