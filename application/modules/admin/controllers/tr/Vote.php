<?php
	class Vote extends Bismillah_Controller{
		public function __construct(){
			parent::__construct() ; 
		}

		public function index(){
			$this->load->view("tr/vote.php") ;
		}

		public function loadgrid(){
			$va	 	= json_decode($this->input->post('request'), true) ;
			$vare 	= array() ; 
			$nvote 	= 0 ;
			$limit	= $va['offset'].",".$va['limit'] ; //limit
			$where 	= array() ; 
			if($va['idactive'] !== "") $where[]	= "v.idactive = '{$va['idactive']}'" ; 
			if($va['idparty'] !== "") $where[]		= "v.idparty = '{$va['idparty']}'" ; 
			$search	= isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
			if($search !== "") $where[]	= "d.code LIKE '{$search}%'" ; 
			$where 	= implode(" AND ", $where) ;  
			$field 	= "v.id, CONCAT(d.code, ' ' , d.name) district, CONCAT(p.code, ' ' , p.name) party, v.vote,
						a.name active" ; 
			$join 	= "	LEFT JOIN mst_active a ON a.id = v.idactive
						LEFT JOIN mst_district d ON d.id = v.iddistrict 
						LEFT JOIN mst_party p ON p.id = v.idparty" ; 
			$dbdata = $this->bdb->select("tr_vote v", $field, $where, $join, "", "a.id DESC, p.id ASC, d.code ASC", $limit) ;
			$dba 	= $this->bdb->select("tr_vote v", "v.id", $where, $join) ; 
			while( $dbrow	= $this->bdb->getrow($dbdata) ){
				$vaset 		= $dbrow ; unset($vaset['id']) ; 
				$vaset['recid']	= $dbrow['id'] ; 
				$vaset['vote']	= number_format($vaset['vote']) ; 
				$nvote 		   += $dbrow['vote'] ; 

				$vaset['cmdedit'] 	= '<button type="button" onClick="bos.trvote.cmdedit(\''.$dbrow['id'].'\')" 
										class="btn btn-success btn-grid">Edit</button>' ;
				$vaset['cmddelete'] = '<button type="button" onClick="bos.trvote.cmddelete(\''.$dbrow['id'].'\')" 
										class="btn btn-danger btn-grid">Delete</button>' ; 
				$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
				$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

				$vare[]		= $vaset ;  
			}

			if($va['offset'] == "0"){
				$dbdata = $this->bdb->select("tr_vote v", "SUM(v.vote) vote", $where, $join) ; 
				if($dbrow	= $this->bdb->getrow($dbdata)){
					$vare[]	= array("w2ui"=>array("summary"=>true), "recid"=>"s1", 
							"vote"=>number_format($dbrow['vote'])) ; 
				}
			}
			
			$vare 	= array("total"=> $this->bdb->rows($dba), "records"=>$vare ) ;
			echo(json_encode($vare)) ; 
		}

		public function init(){
			savesession($this, "ssvote_id", "") ;
		}

		public function saving(){
			$va 	= $this->input->post() ;
			$id 	= getsession($this, "ssvote_id") ;
			$data 	= array("iddistrict"=>$va['iddistrict'], "idparty"=>$va['idparty'], "vote"=>$va['vote'], "description"=>$va['description'],
							"idactive"=>$va['idactive'], "username"=> getsession($this, "username")) ; 
			$this->bdb->update("tr_vote", $data, "id = '{$id}'", "id") ;
			echo(' bos.trvote.init() ; ') ;
			
		}

		public function editing(){
			$va 	= $this->input->post() ; 
			$data 	= $this->bdb->getval("*", "id = '{$va['id']}'", "tr_vote") ;
			if(!empty($data)){
				savesession($this, "ssvote_id", $va['id']) ;
				$odistrict 	= array() ;
				$district 	= $this->bdb->getval("code, name", "id = '{$data['iddistrict']}'", "mst_district") ;
				if(!empty($district)){
					$odistrict[] 	= array("id"=>$data['iddistrict'], "text"=>$district['code'] . " - " . $district['name']) ;
				}

				$oparty 	= array() ; 
				$party	 	= $this->bdb->getval("code, name", "id = '{$data['idparty']}'", "mst_party") ;
				if(!empty($party)){
					$oparty[] 	= array("id"=>$data['idparty'], "text"=>$party['code'] . " - " . $party['name']) ;
				}

				$oactive 	= array() ; 
				$active	 	= $this->bdb->getval("name", "id = '{$data['idactive']}'", "mst_active") ;
				if($active !== ""){
					$oactive[] 	= array("id"=>$data['idactive'], "text"=>$active) ;
				}

				echo('	with(bos.trvote.obj){
							find("#vote").val("'.number_format($data['vote']).'") ;
							find("#description").val("'.$data['description'].'") ;
							find("#iddistrict").sval('.json_encode($odistrict).') ;  
							find("#idparty").sval('.json_encode($oparty).') ; 
							find("#idparty").prop("disabled", false) ; 
							find("#idactive").sval('.json_encode($oactive).') ; 
							find("#idactive").prop("disabled", false) ; 
						}
				') ;
			}
		}

		public function deleting(){
			$va 	= $this->input->post() ;
			$this->bdb->delete("tr_vote", "id = '{$va['id']}'") ;
			echo(' bos.trvote.init() ; ') ; 
		}
	}
?>