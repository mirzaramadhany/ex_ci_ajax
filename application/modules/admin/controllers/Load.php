<?php
class Load extends Bismillah_Controller{
	public function __construct(){
		parent::__construct() ; 
		$this->load->model('loadm') ;
	} 

	public function load_district_code(){
		$q 			= $this->input->get('q') ; 
		$vare		= array() ;
		$dbdata 	= $this->loadm->district_code($q) ;
		while($dbrow= $this->loadm->getrow($dbdata)){
			$vare[]	= array("id"=>$dbrow['code'], "text"=>$dbrow['code'] . ' - ' . $dbrow['name']) ;
		} 
		echo(json_encode($vare)) ; 
	}

	public function load_district_code_nodetail(){
		$q 			= $this->input->get('q') ; 
		$vare		= array() ;
		$dbdata 	= $this->loadm->district_code_nodetail($q) ;
		while($dbrow= $this->loadm->getrow($dbdata)){
			$vare[]	= array("id"=>$dbrow['code'], "text"=>$dbrow['code'] . ' - ' . $dbrow['name']) ;
		} 
		echo(json_encode($vare)) ; 	
	}

	public function load_district_parent(){ 
		$q 			= $this->input->get('q') ; 
		$vare		= array() ;
		$dbdata 	= $this->loadm->district_parent($q) ;
		while($dbrow= $this->loadm->getrow($dbdata)){
			$vare[]	= array("id"=>$dbrow['id'], "text"=>$dbrow['code'] . ' - ' . $dbrow['name']) ;
		} 
		echo(json_encode($vare)) ; 
	}	

	public function load_district(){
		$q 			= $this->input->get('q') ; 
		$vare		= array() ;
		$dbdata 	= $this->loadm->district($q) ;
		while($dbrow= $this->loadm->getrow($dbdata)){
			$vare[]	= array("id"=>$dbrow['id'], "text"=>$dbrow['code'] . ' - ' . $dbrow['name']) ;
		} 
		echo(json_encode($vare)) ; 
	}	

	public function load_party(){
		$q 			= $this->input->get('q') ; 
		$vare		= array() ;
		$dbdata 	= $this->loadm->party($q) ;
		while($dbrow= $this->loadm->getrow($dbdata)){
			$vare[]	= array("id"=>$dbrow['id'], "text"=>$dbrow['code'] . ' - ' . $dbrow['name']) ;
		} 
		echo(json_encode($vare)) ; 
	}	

	public function load_active(){
		$q 			= $this->input->get('q') ; 
		$vare		= array() ;
		$dbdata 	= $this->loadm->active($q) ;
		while($dbrow= $this->loadm->getrow($dbdata)){
			$vare[]	= array("id"=>$dbrow['id'], "text"=> $dbrow['name']) ;
		} 
		echo(json_encode($vare)) ; 
	}

	public function load_target(){
		$q 			= $this->input->get('q') ; 
		$vare		= array() ;
		$dbdata 	= $this->loadm->target($q) ;
		while($dbrow= $this->loadm->getrow($dbdata)){
			$vare[]	= array("id"=>$dbrow['target'], "text"=> $dbrow['name'] . " - " . number_format($dbrow['target']) ) ;
		} 
		if(empty($vare)){
			$vare[]	= array("id"=>$q, "text"=> number_format($q) ) ;	
		}
		echo(json_encode($vare)) ; 	
	}

	public function load_level(){
		$q 			= $this->input->get('q') ; 
		$vare		= array() ;
		$vare[]		= array("id"=>"0000","text"=>"0000 - Administrator") ;
		$dbdata 	= $this->loadm->level($q) ;
		while($dbrow= $this->loadm->getrow($dbdata)){
			$vare[]	= array("id"=>$dbrow['code'], "text"=> $dbrow['code'] . " - " . $dbrow['name'] ) ; 
		}  
		echo(json_encode($vare)) ; 	
	}	
}
?> 