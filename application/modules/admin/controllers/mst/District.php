<?php
	class District extends Bismillah_Controller{
		public function __construct(){
			parent::__construct() ; 
			$this->load->model("mst/districtm") ;
		}

		public function index(){
			$this->load->view("mst/district.php") ;
		}

		public function loadgrid(){
			$va	 	= json_decode($this->input->post('request'), true) ;
			$vare 	= array() ; 
			$vadb 	= $this->districtm->grid($va) ;
			$dbdata = $vadb['db'] ;
 			while( $dbrow	= $this->districtm->getrow($dbdata) ){
				$vaset 		= $dbrow ; unset($vaset['id']) ; 
				$vaset['recid']	= $dbrow['id'] ; 
				$vaset['isvote']= ($vaset['isvote'] == "1") ? "Yes" : "No" ; 

				$vaset['cmdedit'] 	= '<button type="button" onClick="bos.mstdistrict.cmdedit(\''.$dbrow['id'].'\')" 
										class="btn btn-success btn-grid">Edit</button>' ;
				$vaset['cmddelete'] = '<button type="button" onClick="bos.mstdistrict.cmddelete(\''.$dbrow['id'].'\')" 
										class="btn btn-danger btn-grid">Delete</button>' ; 
				$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
				$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

				$vare[]		= $vaset ;  
			}
 
			$vare 	= array("total"=> $vadb['rows'], "records"=>$vare ) ;
			echo(json_encode($vare)) ; 
		}

		public function init(){
			savesession($this, "ssdistrict_id", "") ;
		}

		public function saving(){
			$va 	= $this->input->post() ;
			$id 	= getsession($this, "ssdistrict_id") ;
			$this->districtm->saving($id, $va) ;
			echo(' bos.mstdistrict.init() ; ') ; 
		}

		public function editing(){
			$va 	= $this->input->post() ; 
			$id 	= $va['id'] ;
			$data 	= $this->districtm->getdata($id) ;
			if(!empty($data)){
				savesession($this, "ssdistrict_id", $id) ;
				echo('
					with(bos.mstdistrict.obj){
						find("#ccode").val("'.$data['codeme'].'").attr("readonly", true) ; 
						find("#idcode").html("'.$data['code'].'") ;
						find("#calias").val("'.$data['alias'].'") ;
						find("#cname").val("'.$data['name'].'").focus() ;
						find("#cdescription").val("'.$data['description'].'") ;
						find("#cparent").prop("disabled", true) ;
						find("#cparent").sval('.json_encode($data['oparent']).') ;
					}
					bjs.setopt(bos.mstdistrict.obj, "isvote", "'.$data['isvote'].'") ;
				') ;
			} 
		}

		public function seekcode(){
			$va 	= $this->input->post() ;
			$newcode= str_replace('null', '', $va['newcode']) ; 
			$data 	= $this->districtm->seekcode($newcode) ;
			if($data !== ""){ 
				echo('
					alert("Code used") ;
					bos.mstdistrict.obj.find("#ccode").val("").focus() ;
					bos.mstdistrict.obj.find("#idcode").html("") ;
				') ;
			}
		}

		public function deleting(){
			$va 	= $this->input->post() ;
			$this->districtm->deleting($va['id']) ;
			echo(' bos.mstdistrict.init() ; ') ; 
		}
	}
?>