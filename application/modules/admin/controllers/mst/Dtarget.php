<?php
	class Dtarget extends Bismillah_Controller{
		public function __construct(){
			parent::__construct() ; 
		}

		public function index(){ 
			$this->load->view("mst/dtarget.php") ;
		}

		public function loadgrid(){
			$va	 	= json_decode($this->input->post('request'), true) ;
			$vare 	= array() ; 
			$nfollow = 0 ;
			$limit	= $va['offset'].",".$va['limit'] ; //limit
			$where 	= array() ; 
			if($va['year'] !== "") $where[]	= "tp.year = '{$va['year']}'" ; 
			if($va['idparty'] !== "") $where[]		= "tp.idparty = '{$va['idparty']}'" ; 
			$search	= isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
			if($search !== "") $where[]	= "d.code LIKE '{$search}%'" ; 
			$where 	= implode(" AND ", $where) ;  
			$field 	= "tp.id, CONCAT(d.code, ' ' , d.name) district, CONCAT(p.code, ' ' , p.name) party, tp.target, tp.year" ; 
			$join 	= "	LEFT JOIN mst_district d ON d.id = tp.iddistrict 
						LEFT JOIN mst_party p ON p.id = tp.idparty" ; 
			$dbdata = $this->bdb->select("mst_target_party tp", $field, $where, $join, "", "d.code ASC", $limit) ;
			$dba 	= $this->bdb->select("mst_target_party tp", "tp.id", $where, $join) ; 
			while( $dbrow	= $this->bdb->getrow($dbdata) ){ 
				$vaset 		= $dbrow ; unset($vaset['id']) ; 
				$vaset['recid']		= $dbrow['id'] ;  
				$vaset['target']	= number_format($vaset['target'],2) ; 

				$vaset['cmdedit'] 	= '<button type="button" onClick="bos.mstdtarget.cmdedit(\''.$dbrow['id'].'\')" 
										class="btn btn-success btn-grid">Edit</button>' ;
				$vaset['cmddelete'] = '<button type="button" onClick="bos.mstdtarget.cmddelete(\''.$dbrow['id'].'\')" 
										class="btn btn-danger btn-grid">Delete</button>' ; 
				$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
				$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

				$vare[]		= $vaset ;  
			} 

			if($va['offset'] == "0"){
				$dbdata = $this->bdb->select("mst_target_party tp", "SUM(tp.target) target", $where, $join) ; 
				if($dbrow	= $this->bdb->getrow($dbdata)){
					$vare[]	= array("w2ui"=>array("summary"=>true), "recid"=>"s1", 
									"target"=>number_format($dbrow['target'])) ; 
				}
			}

			$vare 	= array("total"=> $this->bdb->rows($dba), "records"=>$vare ) ;
			echo(json_encode($vare)) ; 
		}

		public function init(){
			savesession($this, "ssmstdtarget_id", "") ;
		}

		public function saving(){
			$va 	= $this->input->post() ;
			$id 	= getsession($this, "ssmstdtarget_id") ;
			$data 	= array("iddistrict"=>$va['iddistrict'], "idparty"=>$va['idparty'], "target"=>$va['target'], 
							"description"=>$va['description'], "year"=>$va['year'], 
							"username"=> getsession($this, "username")) ; 
			$this->bdb->update("mst_target_party", $data, "id = '{$id}'", "id") ;
			echo(' bos.mstdtarget.init() ; ') ;
			
		}

		public function editing(){  
			$va 	= $this->input->post() ; 
			$data 	= $this->bdb->getval("*", "id = '{$va['id']}'", "mst_target_party") ;
			if(!empty($data)){
				savesession($this, "ssmstdtarget_id", $va['id']) ; 
				$odistrict 	= array() ;
				$district 	= $this->bdb->getval("code, name", "id = '{$data['iddistrict']}'", "mst_district") ;
				if(!empty($district)){
					$odistrict[] 	= array("id"=>$data['iddistrict'], "text"=>$district['code'] . " - " . $district['name']) ;
				}

				$oparty 	= array() ; 
				$party	 	= $this->bdb->getval("code, name", "id = '{$data['idparty']}'", "mst_party") ;
				if(!empty($party)){
					$oparty[] 	= array("id"=>$data['idparty'], "text"=>$party['code'] . " - " . $party['name']) ;
				}

				echo('	with(bos.mstdtarget.obj){
							find("#target").val("'.number_format($data['target'],2).'") ;
							find("#description").val("'.$data['description'].'") ;
							find("#year").val("'.$data['year'].'") ;
							find("#iddistrict").sval('.json_encode($odistrict).') ;  
							find("#idparty").sval('.json_encode($oparty).') ; 
							find("#idparty").prop("disabled", false) ; 
						} 
				') ;
			} 
		}

		public function deleting(){
			$va 	= $this->input->post() ;
			$this->bdb->delete("mst_target_party", "id = '{$va['id']}'") ;
			echo(' bos.mstdtarget.init() ; ') ; 
		}
	}
?>