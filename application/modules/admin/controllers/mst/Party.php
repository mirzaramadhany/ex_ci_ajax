<?php
	class Party extends Bismillah_Controller{
		public function __construct(){
			parent::__construct() ; 
		}

		public function index(){
			$this->load->view("mst/party.php") ;
		}

		public function loadgrid(){
			$va	 	= json_decode($this->input->post('request'), true) ;
			$vare 	= array() ; 
			$limit	= $va['offset'].",".$va['limit'] ; //limit
			$where 	= "" ; 
			if($va['cparent'] !== ""){ $where	= "code LIKE '{$va['cparent']}%'" ; }
			$dbdata = $this->bdb->select("mst_party", "id, code, name", $where, "", "", "code ASC", $limit) ;
			while( $dbrow	= $this->bdb->getrow($dbdata) ){
				$vaset 		= $dbrow ; unset($vaset['id']) ; 
				$vaset['recid']	= $dbrow['id'] ; 

				$vaset['cmdedit'] 	= '<button type="button" onClick="bos.mstparty.cmdedit(\''.$dbrow['id'].'\')" 
										class="btn btn-success btn-grid">Edit</button>' ;
				$vaset['cmddelete'] = '<button type="button" onClick="bos.mstparty.cmddelete(\''.$dbrow['id'].'\')" 
										class="btn btn-danger btn-grid">Delete</button>' ; 
				$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
				$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

				$vare[]		= $vaset ;  
			}

			$vare 	= array("total"=> $this->bdb->rows($dbdata), "records"=>$vare ) ;
			echo(json_encode($vare)) ; 
		}

		public function init(){
			savesession($this, "ssparty_id", "") ;
			savesession($this, "ssparty_image", "") ;
		}

		public function saving(){
			$va 	= $this->input->post() ;
			$id 	= getsession($this, "ssparty_id") ;
			$data 	= array("alias"=>$va['calias'], "name"=>$va['cname'], "description"=>$va['cdescription'],
							"username"=>getsession($this, "username") ) ;
			if($id == ""){
				if(!isset($va['cparent'])) $va['cparent']	= "" ; 
				$data['code']	= $va['cparent'] . $va['ccode'] ; 
				$data['codeme']	= $va['ccode'] ;
				$data['codeparent']	= $va['cparent'] ;  	
			} 
			//save image
			$vimage = json_decode(getsession($this, "ssparty_image", "{}"), true) ;
			if(!empty($vimage)){
				$adir 	= $this->config->item('bcore_uploads') ; 
				foreach ($vimage as $key => $img) {
					$vi		= pathinfo($img) ;  
					$dir 	= $adir ;  
					$dir   .=  $key . "_".date("dmy_Hi") ."." . $vi['extension'] ; 
					if(is_file($dir)) @unlink($dir) ; 
					if(@copy($img,$dir)){
						@unlink($img) ; 
						$data['image']	= $dir;  
					}
				}  
			}

			$this->bdb->update("mst_party", $data, "id = '$id'", "id") ;
			echo(' bos.mstparty.init() ; ') ; 
		}

		public function saving_image(){
			$name 	= md5("party".getsession($this, "username")) ; 
			$fcfg	= array("upload_path"=>"./tmp/", "allowed_types"=>"jpg|jpeg|png", "overwrite"=>true, 
							"file_name"=> $name ) ;

			savesession($this, "ssparty_image", "") ;
			$this->load->library('upload', $fcfg) ;
			if ( ! $this->upload->do_upload(0) ){
				echo(' 
					alert("'. $this->upload->display_errors('','') .'") ; 
					bos.mstparty.obj.find("#idlimage").html("") ; 
				') ;
			}else{
				$data 	= $this->upload->data() ;
				$tname 	= str_replace($data['file_ext'], "", $data['client_name']) ;
				$vimage	= array( $tname => $data['full_path']) ; 
				savesession($this, "ssparty_image", json_encode($vimage) ) ;

				echo('
					bos.mstparty.obj.find("#idlimage").html("") ; 
					bos.mstparty.obj.find("#idimage").html("<img src=\"'.base_url("./tmp/" . $name . "?time=". time()).'\" class=\"img-responsive\" />") ;
				') ;
			}
		} 

		public function editing(){
			$va 	= $this->input->post() ; 
			$data 	= $this->bdb->getval("*", "id = '{$va['id']}'", "mst_party") ;
			if(!empty($data)){
				savesession($this, "ssparty_id", $va['id']) ;
				$oparent 	= array() ;
				$parent 	= $this->bdb->getval("name", "id = '{$data['idparent']}'", "mst_party") ;
				if($parent !== ""){
					$oparent[] 	= array("id"=>$data['codeparent'], "text"=>$data['codeparent'] . " - " . $parent) ;
				}

				if(is_file(realpath($data['image'])) ){
					$data['image']	= '<img src="'.base_url($data['image']) . '?time='. time() .'" class="img-responsive" />' ;
				}else{
					$data['image']	= '' ; 
				}
				echo('
					with(bos.mstparty.obj){
						find("#ccode").val("'.$data['codeme'].'").attr("readonly", true) ; 
						find("#idcode").html("'.$data['code'].'") ;
						find("#calias").val("'.$data['alias'].'") ;
						find("#cname").val("'.$data['name'].'").focus() ;
						find("#cdescription").val("'.$data['description'].'") ;
						find("#cparent").prop("disabled", true) ;
						find("#cparent").sval('.json_encode($oparent).') ;
						find("#idimage").html("'.addslashes($data['image']).'") ;
					} 
				') ;
			}
		}

		public function seekcode(){
			$va 	= $this->input->post() ;
			$newcode= str_replace('null', '', $va['newcode']) ; 
			$data 	= $this->bdb->getval("code", "code = '{$newcode}'", "mst_party") ; 
			if($data !== ""){
				echo('
					alert("Code used") ;
					bos.mstparty.obj.find("#ccode").val("").focus() ;
					bos.mstparty.obj.find("#idcode").html("") ;
				') ;
			}
		}

		public function deleting(){
			$va 	= $this->input->post() ;
			$this->bdb->delete("mst_party", "id = '{$va['id']}'") ;
			echo(' bos.mstparty.init() ; ') ; 
		}
	}
?>