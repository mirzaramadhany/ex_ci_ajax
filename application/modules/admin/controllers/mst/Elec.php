<?php
	class Elec extends Bismillah_Controller{
		public function __construct(){
			parent::__construct() ; 
		}

		public function index(){
			$this->load->view("mst/elec.php") ;
		}

		public function loadgrid(){
			$va	 	= json_decode($this->input->post('request'), true) ;
			$vare 	= array() ; 
			$limit	= $va['offset'].",".$va['limit'] ; //limit
			$dbdata = $this->bdb->select("mst_active", "id, name, date_start, date_end", "", "", "", "id DESC", $limit) ;
			while( $dbrow	= $this->bdb->getrow($dbdata) ){
				$vaset 		= $dbrow ; unset($vaset['id']) ; 
				$vaset['recid']		= $dbrow['id'] ; 
				$vaset['date_start']= date("d/m/y H:i", strtotime($vaset['date_start'])) ;
				$vaset['date_end']	= date("d/m/y H:i", strtotime($vaset['date_end'])) ;

				$vaset['cmdedit'] 	= '<button type="button" onClick="bos.mstelec.cmdedit(\''.$dbrow['id'].'\')" 
										class="btn btn-success btn-grid">Edit</button>' ;
				$vaset['cmddelete'] = '<button type="button" onClick="bos.mstelec.cmddelete(\''.$dbrow['id'].'\')" 
										class="btn btn-danger btn-grid">Delete</button>' ; 
				$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
				$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

				$vare[]		= $vaset ;  
			}

			$vare 	= array("total"=> $this->bdb->rows($dbdata), "records"=>$vare ) ;
			echo(json_encode($vare)) ; 
		}

		public function init(){
			savesession($this, "sselec_id", "") ;
		}

		public function saving(){
			$va 	= $this->input->post() ;
			$id 	= getsession($this, "sselec_id") ;
			$dstart = date_2s($va['dstart'], true) ; 
			$dend 	= date_2s($va['dend'], true) ; 

			$data 	= array("name"=>$va['cname'], "description"=>$va['cdescription'],
							"date_start"=>$dstart, "date_end"=>$dend, "username"=>getsession($this, "username") ) ;
			
			$this->bdb->update("mst_active", $data, "id = '$id'", "id") ;
			echo(' bos.mstelec.init() ; ') ; 
		}

		public function editing(){
			$va 	= $this->input->post() ; 
			$data 	= $this->bdb->getval("*", "id = '{$va['id']}'", "mst_active") ;
			if(!empty($data)){
				savesession($this, "sselec_id", $va['id']) ;
				echo('
					with(bos.mstelec.obj){
						find("#cname").val("'.$data['name'].'").focus() ;
						find("#cdescription").val("'.$data['description'].'") ;
						find("#dstart").val("'.date("d-m-Y H:i", strtotime($data['date_start'])).'") ;
						find("#dend").val("'.date("d-m-Y H:i", strtotime($data['date_end'])).'") ;
					} 
				') ;
			}
		}

		public function deleting(){
			$va 	= $this->input->post() ;
			$this->bdb->delete("mst_active", "id = '{$va['id']}'") ;
			echo(' bos.mstelec.init() ; ') ; 
		}
	}
?>