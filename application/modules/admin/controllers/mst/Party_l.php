<?php
	class Party_l extends Bismillah_Controller{
		public function __construct(){
			parent::__construct() ; 
		}

		public function index(){
			$this->load->view("mst/party_l.php") ;
		}

		public function loadgrid(){
			$va	 	= json_decode($this->input->post('request'), true) ;
			$vare 	= array() ; 
			$limit	= $va['offset'].",".$va['limit'] ; //limit
			$dbdata = $this->bdb->select("sys_config", "id, title, val", "type = 'party_l'", "", "", "id DESC", $limit) ;
			while( $dbrow	= $this->bdb->getrow($dbdata) ){
				$vaset 		= $dbrow ; unset($vaset['id']) ; 
				$vaset['recid']	= $dbrow['id'] ; 

				$vaset['cmdedit'] 	= '<button type="button" onClick="bos.mstparty_l.cmdedit(\''.$dbrow['id'].'\')" 
										class="btn btn-success btn-grid">Edit</button>' ;
				$vaset['cmddelete'] = '<button type="button" onClick="bos.mstparty_l.cmddelete(\''.$dbrow['id'].'\')" 
										class="btn btn-danger btn-grid">Delete</button>' ; 
				$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
				$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

				$vare[]		= $vaset ; 
			}

			$vare 	= array("total"=> $this->bdb->rows($dbdata), "records"=>$vare ) ;
			echo(json_encode($vare)) ; 
		}

		public function init(){
			savesession($this, "party_l_id", "") ;
		} 

		public function saving(){
			$va	 	= $this->input->post() ;
			$id 	= getsession($this, "party_l_id") ;
			$where 	= "id = '$id'" ;
			$data 	= array("title"=>$va['cname'], "val"=>$va['nlen'], "type"=>"party_l") ;
			$this->bdb->update("sys_config", $data, $where, 'id') ;
			echo(' bos.mstparty_l.init(); ') ;
		}

		public function editing(){
			$va	 	= $this->input->post() ;
			$data 	= $this->bdb->getval("title, val", "id = '{$va['id']}'", "sys_config") ;
			if(!empty($data)){
				savesession($this, "party_l_id", $va['id']) ;
				echo('
					with(bos.mstparty_l.obj){
						find("#cname").val("'.$data['title'].'") ;
						find("#nlen").val("'.$data['val'].'") ;
						find("#cname").focus() ; 
					}
				') ; 
			}
		}

		public function deleting(){
			$va	 	= $this->input->post() ;
			$this->bdb->delete("sys_config", "id = '{$va['id']}'") ;
			echo(' bos.mstparty_l.init(); ') ; 
		}
	}
?>