<?php
	class Target extends Bismillah_Controller{
		public function __construct(){
			parent::__construct() ; 
		}

		public function index(){
			$this->load->view("mst/target.php") ;
		}

		public function loadgrid(){
			$va	 	= json_decode($this->input->post('request'), true) ;
			$vare 	= array() ; 
			$limit	= $va['offset'].",".$va['limit'] ; //limit
			$dbdata = $this->bdb->select("mst_target", "id, name, target", "", "", "", "id DESC", $limit) ;
			while( $dbrow	= $this->bdb->getrow($dbdata) ){
				$vaset 		= $dbrow ; unset($vaset['id']) ; 
				$vaset['recid']		= $dbrow['id'] ; 
				$vaset['target']= number_format($vaset['target']) ;

				$vaset['cmdedit'] 	= '<button type="button" onClick="bos.msttarget.cmdedit(\''.$dbrow['id'].'\')" 
										class="btn btn-success btn-grid">Edit</button>' ;
				$vaset['cmddelete'] = '<button type="button" onClick="bos.msttarget.cmddelete(\''.$dbrow['id'].'\')" 
										class="btn btn-danger btn-grid">Delete</button>' ; 
				$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
				$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

				$vare[]		= $vaset ;  
			}

			$vare 	= array("total"=> $this->bdb->rows($dbdata), "records"=>$vare ) ;
			echo(json_encode($vare)) ; 
		}

		public function init(){
			savesession($this, "sstarget_id", "") ;
		}

		public function saving(){  
			$va 	= $this->input->post() ;
			$id 	= getsession($this, "sstarget_id") ;

			$data 	= array("name"=>$va['name'], "description"=>$va['description'],
							"target"=>$va['target'], "username"=>getsession($this, "username") ) ;
			
			$this->bdb->update("mst_target", $data, "id = '$id'", "id") ;
			echo(' bos.msttarget.init() ; ') ; 
		}
 
		public function editing(){
			$va 	= $this->input->post() ; 
			$data 	= $this->bdb->getval("*", "id = '{$va['id']}'", "mst_target") ;
			if(!empty($data)){ 
				savesession($this, "sstarget_id", $va['id']) ;
				echo('
					with(bos.msttarget.obj){
						find("#name").val("'.$data['name'].'").focus() ;
						find("#description").val("'.$data['description'].'") ;
						find("#target").val("'.$data['target'].'") ;
					} 
				') ;
			}
		}

		public function deleting(){
			$va 	= $this->input->post() ;
			$this->bdb->delete("mst_target", "id = '{$va['id']}'") ;
			echo(' bos.msttarget.init() ; ') ; 
		}
	}
?>