<?php
	class Username extends Bismillah_Controller{
		public function __construct(){
			parent::__construct() ; 
		}

		public function index(){
			$this->load->view("config/username.php") ;
		}

		public function loadgrid(){
			$va	 	= json_decode($this->input->post('request'), true) ;
			$vare 	= array() ; 
			$limit	= $va['offset'].",".$va['limit'] ; //limit
			$dbdata = $this->bdb->select("sys_username", "username, fullname", "", "", "", "username DESC", $limit) ;
			while( $dbrow	= $this->bdb->getrow($dbdata) ){
				$vaset 		= $dbrow ;  
				$vaset['recid']		= $dbrow['username'] ; 
				
				$vaset['cmdedit'] 	= '<button type="button" onClick="bos.username.cmdedit(\''.$dbrow['username'].'\')" 
										class="btn btn-success btn-grid">Edit</button>' ;
				$vaset['cmddelete'] = '<button type="button" onClick="bos.username.cmddelete(\''.$dbrow['username'].'\')" 
										class="btn btn-danger btn-grid">Delete</button>' ; 
				$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
				$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

				$vare[]		= $vaset ;  
			}

			$vare 	= array("total"=> $this->bdb->rows($dbdata), "records"=>$vare ) ;
			echo(json_encode($vare)) ; 
		}

		public function init(){
		}

		public function seekusername(){
			$va 	= $this->input->post() ;
			if($data= $this->bdb->getval("username", "username = '{$va['username']}'", "sys_username")){
				echo(' bos.username.obj.find("#username").val("") ;  ') ;
			}
		} 

		public function saving(){
			$va 		= $this->input->post() ;
			$username 	= $va['username'] ; 
			if( $dblast = $this->bdb->getval("*", "username = '$username'", "sys_username") ){
				$dblast['data_var']	= ($dblast['data_var'] !== "") ? json_decode($dblast['data_var'], true) : array() ; 	
			}
			if(empty($dblast)){
				$dblast = array("password"=>"", "data_var"=>array('ava'=>"")) ;
			}
			
			$data 		= array("username"=>$va['username'], "fullname"=>$va['fullname']) ;
			$data['data_var']	= array("district"=>$va['district'], "ava"=>$dblast['data_var']['ava']) ; 
 
			if($va['password'] !== ""){
				$data['password']	= pass_crypt($va['password']) . $va['level'] ; 
			}else{
				$data['password']	= substr($dblast['password'], 0, strlen($dblast['password'])-4) . $va['level']; 
			}

			$vimage 		= json_decode(getsession($this, "ssusername_image", "{}"), true) ;
			if(!empty($vimage)){
				$adir 	= $this->config->item('bcore_uploads') ; 
				foreach ($vimage as $key => $img) {
					$vi		= pathinfo($img) ;  
					$dir 	= $adir ;  
					$dir   .=  $key . "_".date("dmy_Hi") ."." . $vi['extension'] ; 
					if(is_file($dir)) @unlink($dir) ; 
					if(@copy($img,$dir)){
						@unlink($img) ; 
						@unlink($dblast['data_var']['ava']) ; 
						$data['data_var']['ava']	= $dir;  
					}
				}  
			}
			$data['data_var']	= json_encode($data['data_var']) ; 
			
			$this->bdb->update("sys_username", $data, "username = '$username'", "username") ;
			echo(' bos.username.init() ; ') ; 
		}

		public function saving_image(){
			$fcfg	= array("upload_path"=>"./tmp/", "allowed_types"=>"jpg|jpeg|png", "overwrite"=>true) ;

			savesession($this, "ssusername_image", "") ;
			$this->load->library('upload', $fcfg) ;
			if ( ! $this->upload->do_upload(0) ){
				echo(' 
					alert("'. $this->upload->display_errors('','') .'") ; 
					bos.mstparty.obj.find("#idlimage").html("") ; 
				') ;
			}else{
				$data 	= $this->upload->data() ;
				$tname 	= str_replace($data['file_ext'], "", $data['client_name']) ;
				$vimage	= array( $tname => $data['full_path']) ; 
				savesession($this, "ssusername_image", json_encode($vimage) ) ;

				echo(' 
					bos.username.obj.find("#idlimage").html("") ; 
					bos.username.obj.find("#idimage").html("<img src=\"'.base_url("./tmp/" . $data['client_name'] . "?time=". time()).'\" class=\"img-responsive\" />") ;
				') ;
			}
		}

		public function editing(){ 
			$va 	= $this->input->post() ; 
			$data 	= $this->bdb->getval("*", "username = '{$va['username']}'", "sys_username") ;
			if(!empty($data)){
				$image 		= "" ; 
				$sdistrict 	= array() ;
				$slevel 	= array() ; 
				$data_var	= ($data['data_var'] !== "") ? json_decode($data['data_var'], true) : array() ;
				if(isset($data_var['ava'])){
					$image 	= '<img src=\"'.base_url($data_var['ava']).'\" class=\"img-responsive\"/>' ;
				}
				if(isset($data_var['district'])){
					$sdistrict[]	= array("id"=>$data_var['district'],
									"text"=>$this->bdb->getval("name", "id = '{$data_var['district']}'", "mst_district")) ;	
				}
				$level 		= substr($data['password'], -4) ; 
				$slevel[] 	= array("id"=>$level, "text"=> $level . " - " . $this->bdb->getval("name", "code = '{$level}'", "sys_username_level")) ;

 				echo('
					with(bos.username.obj){
						find("#username").val("'.$data['username'].'").attr("readonly", true) ;
						find("#fullname").val("'.$data['fullname'].'").focus() ;
						find("#district").sval('.json_encode($sdistrict).') ;
						find("#level").sval('.json_encode($slevel).') ;
						find("#idimage").html("'.$image.'")
					} 
				') ;
			}
		}

		public function deleting(){
			$va 	= $this->input->post() ; 
			$this->bdb->delete("sys_username", "username = '{$va['username']}'") ;
			echo(' bos.username.init() ; ') ; 
		}
	}
?>