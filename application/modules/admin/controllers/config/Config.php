<?php
class Config extends Bismillah_Controller{
	public function __construct(){
		parent::__construct() ; 
	}

	public function index(){
		$data 		= array("app_title"=> getsession($this, "app_title") , "pro_1_seat"=> $this->bdb->getconfig("pro_1_seat"),
							"app_logo"=>"", "app_login_image"=>"", "app_party"=>array() ) ;
		$app_logo	= $this->bdb->getconfig("app_logo") ;
		if($app_logo !== ""){
			$data['app_logo']	= '<img src="'.$app_logo.'" class="img-responsive" style="margin: 0 auto;"/>' ;
		}
		$app_login_image= $this->bdb->getconfig("app_login_image") ;
		if($app_login_image !== ""){
			$data['app_login_image']= '<img src="'.$app_login_image.'" class="img-responsive" style="margin: 0 auto;"/>' ;
		}
		$app_party	= getsession($this, "app_party") ;
		if(!empty($app_party)){
			$data['app_party'][]	= array("id"=>$app_party['id'], "text"=>$app_party['name']) ;
		}

		//remove session
		savesession($this, "ssconfig_app_logo", "" ) ;
		savesession($this, "ssconfig_app_login_image", "" ) ;
		$this->load->view("config/config", $data) ; 
	}

	public function saving_image($cfg){
		savesession($this, "ssconfig_" . $cfg, "") ;

		$fcfg	= array("upload_path"=>"./tmp/", "allowed_types"=>"jpg|jpeg|png", "overwrite"=>true, 
						"file_name"=> $cfg ) ;
		$this->load->library('upload', $fcfg) ;
 
		if ( ! $this->upload->do_upload(0) ){
			echo(' 
				alert("'. $this->upload->display_errors('','') .'") ; 
				bos.config.obj.find("#idl'.$cfg.'").html("") ; 
			') ;
		}else{
			$data 	= $this->upload->data() ; 
			$tname 	= str_replace($data['file_ext'], "", $data['client_name']) ;
			$vimage	= array( $tname => $data['full_path']) ; 
			savesession($this, "ssconfig_" . $cfg, json_encode($vimage) ) ;

			echo(' 
				bos.config.obj.find("#idl'.$cfg.'").html("") ; 
				bos.config.obj.find("#id'.$cfg.'").html("<img src=\"'.base_url("./tmp/" . $cfg . "?time=". time()).'\" class=\"img-responsive\" style=\"margin:0 auto;\"/>") ;
			') ; 
		}
	}

	public function saving(){
		$va 	= $this->input->post() ;
		foreach ($va as $key => $value) {
			$this->bdb->saveconfig($key, $value) ;
		}

		//save image
		$adir 	= $this->config->item('bcore_uploads') ; 
		$upload = array("app_logo"=>getsession($this, "ssconfig_app_logo"), 
						"app_login_image"=>getsession($this, "ssconfig_app_login_image")) ; 
 		foreach ($upload as $key => $value) {
 			if($value !== ""){
 				$value 	= json_decode($value, true) ; 
 				foreach ($value as $tkey => $img) {
 					$vi		= pathinfo($img) ; 
 					$dir 	= $adir ;   
					$dir   .=  $tkey . "." . $vi['extension'] ; 
					if(is_file($dir)) @unlink($dir) ; 
					if(@copy($img,$dir)){ 
						@unlink($img) ; 
						@unlink($this->bdb->getconfig($key)) ;
						$this->bdb->saveconfig($key, $dir) ; 
					}  
 				}
 			}
 		}
		echo('alert("Data Configuration Saved"); ') ;
	}
}
?>