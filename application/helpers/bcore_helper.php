<?php
    defined('BASEPATH') OR exit('No direct script access allowed') ; 

    date_default_timezone_set('Asia/Jakarta') ; 

    function menu_all_laod($o){
        $ssmenu     = "sys_module_menu" ;
        $vamenu     = getsession($o, $ssmenu, "{}") ;
        $vamenu     = json_decode($vamenu, true) ;
        if(empty($vamenu)){
            $dbdata     = $o->bdb->select("sys_module", "name, path", "", "", "", "id ASC") ;
            while($dbrow= $o->bdb->getrow($dbdata)){
                $vamenu[$dbrow['name']] = APPPATH . "modules/" . $dbrow['path'] . "/menu.php";
            }  
            savesession($o, $ssmenu, json_encode($vamenu)) ;  
        }
        return $vamenu ; 
    }

    function menu_get_data($vamenu, $md5, $lchild=false){
        $arr    = array() ;
        foreach ($vamenu as $key => $value) {
            if(empty($arr)){
                if(isset($value["children"])){
                    $arr     = menu_get_data($value["children"],$md5) ; 
                }
                if( $key == $md5 ){
                    if($lchild && isset($value['children'])){
                        $arr     = $value['children'] ;    
                    }else{
                        $arr     = $value ;    
                    }
                    break ; 
                }
            }
        }
        return $arr ; 
    }

    function menu_get($o, $path, $ssmenu="bmenu", $cmodule_name=''){
        $vamenu    = json_decode(getsession($o, $ssmenu, "{}"), true) ;
        if(empty($vamenu)){
            if(is_file($path)){
                $vafile     = file($path) ;
                $vakey_pos  = array() ;  
                foreach ($vafile as $ckeyfile => $menu) {
                    $ci     = strpos($menu, "#") ; 
                    $lphp   = strpos($menu, "<?php") ; 
                    if($ci === false && trim($menu) !== "" && $lphp === false){
                        $np         = strpos($menu,"[") ;    
                        $armenu     = menu_to_array($o, $menu, $cmodule_name) ;  
                        if($np == 0){ $vakey_pos  = array() ;}

                        $key      = $armenu['md5'] ; 
                        $vakey_pos[$np] = $key ;  

                        menu_set_array($vamenu,$vakey_pos,$np,$key,$armenu) ;
                    }
                }
                savesession($o, $ssmenu, json_encode($vamenu)) ;
            }
        }
        return $vamenu ; 
    }

    function menu_set_array(&$vamenu, $vakey_pos, $np, $key, $armenu){
        if($np > 0){  
            $lv     = true ; 
            $va_var     = "" ; 
            ksort($vakey_pos) ; 
            foreach ($vakey_pos as $key_pos => $value_pos) {
                if($lv && $key_pos == $np ) $lv   = false ; 
                if($lv){
                    if(isset($vamenu[$value_pos])){
                        $va_var .= '["'.$value_pos.'"]' ; 
                    }else{
                        $va_var .= '["children"]["'.$value_pos.'"]' ;  
                    }
                }
            }
            eval('
                $vamenu'.$va_var.'["children"]["'.$key.'"] = $armenu ;  
            ') ; 

        }else{
            $vamenu[$key] = $armenu ;
        } 
    }

	function menu_to_array($o, $menu, $module_name=''){
        eval('$menu = array' . str_replace("[","(", str_replace("]",")",$menu)) . ';' ) ; 
        if( $o->config->item('bcore_menu_version') == "1"){ 
            $md5    = isset($menu[2]) ? ( $menu[2] !== "" ? $menu[2] : $menu[1]) : $menu[1] ;
            $varr   = array("module"=>$module_name,
                            "name"=>$menu[0], 
                            "md5"=>md5($md5), 
                            "obj"=>$menu[1], 
                            "loc"=>isset($menu[2]) ? $menu[2] : "", 
                            "icon"=>isset($menu[3]) ? $menu[3] : "ion-filing"
                            ) ;
        }

        return $varr ; 
	}

	function pass_crypt($pass){
		return sha1((md5($pass.md5($pass)) . ord('b') . ord('b') . "bismillah") ) ;
	}

	function dir_create($dir){
		@mkdir($dir,0777,true) ; 
	}

	function dir_delete($dir){
		$files = array_diff(scandir($dir), array('.','..')); 
	    foreach ($files as $file) { 
	      (is_dir($dir . "/" . $file)) ? dir_delete($dir . "/" . $file) : unlink($dir . "/" . $file); 
	    }  
	    return rmdir($dir); 
	}

	function isjson($string){
    	json_decode($string) ; 
    	return (json_last_error() == JSON_ERROR_NONE);
    }

    function checktext(){

    }

    function date_2s($date, $ltime=false){
        $re     = $date ; 
        $vdate  = explode(" ", $date) ; 
        if($ltime){
            $vdate_d= explode("-", $vdate[0]) ;
            $re     = $vdate_d[2] . "-" . $vdate_d[1] . "-" . $vdate_d[0] . " " . $vdate[1] ; 
        }else{
            $vdate_d= explode("-", $vdate[0]) ;
            $re     = $vdate_d[2] . "-" . $vdate_d[1] . "-" . $vdate_d[0] ; 
        }
        return $re ;
    }

    function date_eom(){

    }

    function date_bom(){

    }

    function savesession($o ,$name, $val){
    	$name 	= md5($name . $o->input->user_agent() . $o->config->item('encryption_key') ) ; 
        if($val !== ""){
            $o->session->set_userdata($name, $val) ;
        }else{
            $o->session->unset_userdata($name) ;
        }
    }

    function getsession($o, $name, $def=''){
    	$name 	= md5( $name . $o->input->user_agent() . $o->config->item('encryption_key') ) ; 
        $re     = $o->session->userdata($name) ; 
        if($re == "") $re = $def ; 
    	return $re ; 
    }

    function savecookie($o ,$name, $val){

    }

    function getcookie($o, $name){

    }
?> 